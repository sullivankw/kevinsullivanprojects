/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.addressbook.dao.AddressBookDao;
import com.mycompany.addressbook.dto.Address;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author mymac
 */
public class AddressBookDaoImplTests {
    
    AddressBookDao addressBookdao;
    
    Address testA = new Address();
    
    public AddressBookDaoImplTests() {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("testspring-persistence.xml");
        addressBookdao = ctx.getBean("AddressBookDao", AddressBookDao.class); 

    }
    
    @Before
    public void setUp() {
        
        testA = new Address();
        testA.setFirstName("Phil");
        testA.setLastName("Cheese");
        testA.setStreetAddress("19 Ave");
        testA.setCity("Boise");
        testA.setState("ID");
        testA.setZip("77777");
        testA.setId(1);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCreateMethod() {
  
        Address a = addressBookdao.create(testA);
        System.out.println("Running test Not Null  is true");
        Assert.assertNotNull(a);
        Assert.assertNotNull(a.getFirstName());
        Assert.assertEquals("Create test action", "Phil", a.getFirstName());
        Assert.assertEquals("Boise", a.getCity());

    
    }

    @Test
    public void testEditMethod() {

        testA.setCity("Austin");
        testA.setFirstName("Jon");

        addressBookdao.update(testA);
        System.out.println("Running test Not Null  is true");
        Assert.assertNotNull(testA);
        Assert.assertNotNull(testA.getFirstName());
        Assert.assertEquals("Create test action", "Jon", testA.getFirstName());
        Assert.assertEquals("Austin", testA.getCity());
        Assert.assertEquals("Cheese", testA.getLastName());


    }


}
