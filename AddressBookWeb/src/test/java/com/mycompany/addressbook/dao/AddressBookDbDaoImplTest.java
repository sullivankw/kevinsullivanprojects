package com.mycompany.addressbook.dao;

import com.mycompany.addressbook.dto.Address;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

/**
 * Created by mymac on 10/1/16.
 */
public class AddressBookDbDaoImplTest {
    AddressBookDao addressBookdao;

    Address testA = new Address();

    public AddressBookDbDaoImplTest() {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("testspring-persistence.xml");
        addressBookdao = ctx.getBean("AddressBookDao", AddressBookDao.class);

    }

    @Before
    public void setUp() {

        testA = new Address();
        testA.setFirstName("Phil");
        testA.setLastName("Cheese");
        testA.setStreetAddress("19 Ave");
        testA.setCity("Boise");
        testA.setState("ID");
        testA.setZip("77777");
        testA.setId(1);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testReadMethod() {

        Address j = addressBookdao.read(5);
        System.out.println("Running test Not Null  is true");
        Assert.assertNotNull(j.getFirstName());
        Assert.assertEquals("Create test action", "Jill", j.getFirstName());

    }

    @Test
    public void testCreateMethod() {

        Address a = addressBookdao.create(testA);
        System.out.println("Running test Not Null  is true");
        Assert.assertNotNull(a);
        Assert.assertNotNull(a.getFirstName());
        Assert.assertEquals("Create test action", "Phil", a.getFirstName());
        Assert.assertEquals("Boise", a.getCity());


    }

    @Test
    public void testEditMethod() {



        Address editAddress = addressBookdao.read(8);

        editAddress.setCity("Pitt");
        editAddress.setFirstName("Jon");

        addressBookdao.update(editAddress);
        System.out.println("Running test Not Null  is true");
        Assert.assertNotNull(editAddress);
        Assert.assertNotNull(editAddress.getFirstName());
        Assert.assertEquals("Create test action", "Jon", editAddress.getFirstName());
        Assert.assertEquals("Pitt", editAddress.getCity());
        Assert.assertEquals("Smelly", editAddress.getLastName());


    }

    @Test
    public void testDeleteMethod() {

       // deletes, but still references spot in memory as written below...so fails
//        Address deleteAddress = addressBookdao.read(6);
//        Assert.assertEquals("Create test action", "Jim", deleteAddress.getFirstName());

//        addressBookdao.delete(deleteAddress);
//        Assert.assertNull(deleteAddress);

    }




}