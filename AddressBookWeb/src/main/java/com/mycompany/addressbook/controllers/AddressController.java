/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.controllers;

import com.mycompany.addressbook.dao.AddressBookDao;
import com.mycompany.addressbook.dto.Address;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author mymac
 */
@Controller
@RequestMapping(value = "/address")
public class AddressController {

    private AddressBookDao addressBookDao;

    @Inject
    public AddressController(AddressBookDao addressBookDao) {
        this.addressBookDao = addressBookDao;

    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Address show(@PathVariable("id") Integer addressId) {

        Address c = addressBookDao.read(addressId);

        

        return c;

    }

    @RequestMapping(value = "showdelete/{id}", method = RequestMethod.GET)
    public String deleteShow(@PathVariable("id") Integer addressId, Map model) {

        Address c = addressBookDao.read(addressId);
        model.put("address", c);

        return "showdelete";

    }

    @RequestMapping(value ="", method = RequestMethod.POST)
    @ResponseBody
    public Address add(@Valid @RequestBody Address address) {

        address.getFirstName();

        Address a = addressBookDao.create(address);

        return a;
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") Integer addressId, Map model) {

        Address c = addressBookDao.read(addressId);

        model.put("address", c);

        return "edit";

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Address editSubmit(@Valid @RequestBody Address address) {

        addressBookDao.update(address);

        return address;

    }

    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Integer addressId, Map model) {

        Address c = addressBookDao.read(addressId);

        model.put("address", c);

        return "delete";

    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteSubmit(@RequestBody Address address) {

        addressBookDao.delete(address);


    }    

}
