/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.controllers;

import com.mycompany.addressbook.dao.AddressBookDao;
import com.mycompany.addressbook.dto.Address;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 * @author mymac
 */
@Controller
public class HomeController {
    private AddressBookDao addressBookDao;
    
    public HomeController(AddressBookDao addressBookDao) {
        this.addressBookDao = addressBookDao;
    }

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Map model) {

        List<Address> addresses = addressBookDao.all();
        
        model.put("address", new Address());
        model.put("addressList", addresses);
        
        return "home";
        
    } 
    
    
}
