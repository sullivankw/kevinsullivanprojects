package com.mycompany.addressbook.aop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mymac on 9/23/16.
 */
public class ValidationErrorContainer {

    private List<ValidationError> errors = new ArrayList();

    public List<ValidationError> getErrors() {
        return errors;
    }

    public void addError(ValidationError error) {
        errors.add(error);
    }




}