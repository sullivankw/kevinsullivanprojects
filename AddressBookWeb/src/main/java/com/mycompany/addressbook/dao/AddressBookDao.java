/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.dao;

import com.mycompany.addressbook.dto.Address;
import java.util.List;

/**
 *
 * @author mymac
 */
public interface AddressBookDao {
    
    List<Address> all();

    Address create(Address address);

    List<Address> decode();

    void delete(Address address);

    void encode();

    Address read(Integer id);

    void update(Address Address);
    
    List<Address> search(String lastName);

}
