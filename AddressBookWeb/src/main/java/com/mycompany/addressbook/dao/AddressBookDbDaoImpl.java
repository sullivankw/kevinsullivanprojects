package com.mycompany.addressbook.dao;

import com.mycompany.addressbook.dto.Address;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.tree.RowMapper;
import javax.swing.tree.TreePath;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by mymac on 9/30/16.
 */
public class AddressBookDbDaoImpl implements AddressBookDao {

    private static final String SQL_SELECT_ADDRESS = "SELECT * FROM addressbookweb.addresslist WHERE id=?";

    private static final String SQL_DELETE_ADDRESS = "DELETE FROM addressbookweb.addresslist\n" +
            "WHERE id = ?";

    private static final String SQL_SELECT_ALL_ADDRESSES = "SELECT * FROM addressbookweb.addresslist";

    private static final String SQL_UPDATE_ADDRESS = "UPDATE addressbookweb.testaddresslist SET street_address=?, first_name=?," +
            " last_name=?, city=?, state=? WHERE id=?";

    private static final String SQL_CREATE_ADDRESS = "INSERT INTO addressbookweb.addresslist (street_address, first_name, " +
            "last_name, city, state, zip) VALUES (?, ?, ?, ?, ?, ?)";

    private  JdbcTemplate jdbcTemplate;

    public AddressBookDbDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Address> all() {
        List<Address> addresses = jdbcTemplate.query(SQL_SELECT_ALL_ADDRESSES, new AddressMapper());
        return addresses;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Address create(Address address) {
        jdbcTemplate.update(SQL_CREATE_ADDRESS, address.getStreetAddress(), address.getFirstName(), address.getLastName(), address.getCity(), address.getState(), address.getZip());
        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        address.setId(newId);
        return address;
    }

    @Override
    public List<Address> decode() {
        return null;
    }

    @Override
    public void delete(Address address) {
        jdbcTemplate.update(SQL_DELETE_ADDRESS, address.getId());

    }

    @Override
    public void encode() {

    }

    @Override
    public Address read(Integer id) {
       Address a = jdbcTemplate.queryForObject(SQL_SELECT_ADDRESS, new AddressMapper(), id);

        return a;
    }

    @Override
    public void update(Address Address) {

        jdbcTemplate.update(SQL_UPDATE_ADDRESS, Address.getStreetAddress(), Address.getFirstName(), Address.getLastName(), Address.getCity(), Address.getState(), Address.getZip());

    }

    @Override
    public List<Address> search(String lastName) {
        return null;
    }

    private static final class AddressMapper implements org.springframework.jdbc.core.RowMapper<Address> {

        @Override
        public Address mapRow(ResultSet resultSet, int i) throws SQLException {

            Address a = new Address();
            a.setId(resultSet.getInt("id"));
            a.setCity(resultSet.getString("city"));
            a.setState(resultSet.getString("state"));
            a.setFirstName(resultSet.getString("first_name"));
            a.setLastName(resultSet.getString("last_name"));
            a.setStreetAddress(resultSet.getString("street_address"));
            a.setZip(resultSet.getString("zip"));

            return a;

        }
    }


}
