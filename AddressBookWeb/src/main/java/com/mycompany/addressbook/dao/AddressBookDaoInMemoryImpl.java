/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.dao;

import com.mycompany.addressbook.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mymac
 */
public class AddressBookDaoInMemoryImpl implements AddressBookDao {
    private List<Address> addresses;
    private static final String FILENAME = "addressTest.txt";
    private static final String TOKEN = "::"; 
    
    public AddressBookDaoInMemoryImpl() {
        addresses = decode();
    }

    @Override
    public List<Address> all() {
        return new ArrayList(addresses);
    }

    @Override
    public Address create(Address address) {
         
        address.setId(addresses.size() + 1);
        
        addresses.add(address);
        
        encode();
        
        return address;  
    }

    @Override
    public List<Address> decode() {
        List<Address> tempAddressList = new ArrayList();
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));
            
            while(sc.hasNextLine() ) {
            
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split(TOKEN);
                
                Address myAddress = new Address();
                
                myAddress.setId(Integer.parseInt(stringParts[0]));
                myAddress.setFirstName(stringParts[1]);
                myAddress.setLastName(stringParts[2]);
                myAddress.setStreetAddress(stringParts[3]);
                myAddress.setCity(stringParts[4]);
                myAddress.setState(stringParts[5]);
                myAddress.setZip(stringParts[6]);

                tempAddressList.add(myAddress);

            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tempAddressList;
    }

    @Override
    public void delete(Address address) {

        Iterator<Address> iter = addresses.iterator();

        while (iter.hasNext())
        {
            Address c = iter.next();

            if (c.getId()== address.getId()) {

                iter.remove();
            }
        }

        encode();
    } 

   

    @Override
    public void encode() {
       PrintWriter out = null;
        
        try{
                out = new PrintWriter(new FileWriter(FILENAME));
        
                for(Address s : addresses) 
        {
        
                out.print(s.getId());
                out.print(TOKEN);
                       
                out.print(s.getFirstName());
                out.print(TOKEN);
                
                out.print(s.getLastName());
                out.print(TOKEN);
                
                out.print(s.getStreetAddress());
                out.print(TOKEN);
                
                out.print(s.getCity());
                out.print(TOKEN); 
                      
                out.print(s.getState());
                out.print(TOKEN);
                
                out.print(s.getZip());
                out.print("\n");
            
        }   
                
                out.flush();

        
        } catch (IOException ex) {
            
            
        } finally {

            out.close();
        }

    }


    @Override
    public Address read(Integer id) {
        List<Address> result = new ArrayList<>();

        for (Address a :addresses)
        {
            if (a.getId()== id) {
                return a;

            }
        }

        return null;
    }  
    

    @Override
    public void update(Address address) {

    ListIterator litr = addresses.listIterator();
    
    while (litr.hasNext()) {
        
        Address c = (Address) litr.next();
        
        if (c.getId() == address.getId()) {
            litr.set(address);
        }
        
    }
        
        encode();


    }  

    @Override
    public List<Address> search(String lastName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}