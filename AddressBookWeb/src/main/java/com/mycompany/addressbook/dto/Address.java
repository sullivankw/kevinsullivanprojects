/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author mymac
 */
public class Address {
    
    int id;
    @NotEmpty(message="Please enter a street address")
    String streetAddress;
    @NotEmpty(message="Please enter a first name")
    String firstName;
    @NotEmpty(message="Please enter a last name")
    String lastName;
    @NotEmpty(message="Please enter a city")
    String city;
    @NotEmpty(message="Please enter a state")
    String state;
    @NotEmpty(message="Please enter a zip")
    String zip;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
}

