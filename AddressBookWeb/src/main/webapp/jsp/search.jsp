<%-- 
    Document   : navbar
    Created on : Sep 14, 2016, 1:39:02 PM
    Author     : mymac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <hr>   
     
        <title>Search Page</title>
    </head>
    <body>
       <h1><center>Address Book</center></h1>
            <nav>
                   <ul class="nav nav-tabs">
                    <li role="presentation" ><a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                  
                </ul>
            </nav>
                    
                   
                    <div class="container" >
                 
                <div class="form-group">
                    <label for="inputCity3" class="col-sm-2 control-label"> City:</label>
                    <div class="col-sm-10">
                        <input type="text" name="studio"  class="form-control" id="inputCity3" />
                    </div>
                </div>            
                <div class="form-group">
                    <label for="inputState3" class="col-sm-2 control-label"> State:</label>
                    <div class="col-sm-10">
                        <input type="text" name="userRating"  class="form-control" id="inputState3" />
                    </div>
                </div> 

                </div>                  
                  <div class="form-group">
                     <div class="col-sm-offset-2 col-sm-10">
                        
                        <button type="submit" class="btn btn-primary">Search</button>
                     </div>
                  </div>
    </div>
</form>

       
            
    </body>
</html>