$(document).ready(function() {


    $('#addAddressButton').on('click', function(e) {
        console.log("got here");

        $('#create-address-validation-errors').text('');

        e.preventDefault();

        var myAddress = {
            firstName: $('#inputFirstName3').val(),
            lastName: $('#inputLastName3').val(),
            streetAddress: $('#inputStreetAddress3').val(),
            city: $('#inputCity3').val().toUpperCase(),
            state: $('#inputState3').val(),
            zip: $('#inputZip3').val()


        };

        console.log(myAddress.city +"line 22");
        var myAddressData = JSON.stringify(myAddress);
        console.log(contextRoot);



        $.ajax({
            url: contextRoot + "/address",
            type: "POST",
            data: myAddressData,
            dataType: "json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");


            },
            success: function(data, status) {

                var tableRow = buildAddressRow(data);
                console.log(myAddress.city +"line 42");

                console.log(tableRow);

                $('#addressTable5').append($(tableRow));

                $('#inputFirstName3').val(''),
                    $('#inputLastName3').val(''),
                    $('#inputCompany3').val(''),
                    $('#inputStreetAddress3').val(''),
                    $('#inputCity3').val(''),
                    $('#inputState3').val(''),
                    $('#inputZip3').val('')

            },
            error: function(data, status) {
                var errors = data.responseJSON.errors;

                $('#create-address-validation-errors').text('');

                $.each(errors, function(index, error) {

                    $('#create-address-validation-errors').append('<li>'+ error.message + '</li>');

                });

            }


        });

    });



    $('#editAddressButton').on('click', function(e) {

        $('#edit-address-validation-errors').text('');
        e.preventDefault();

        var myAddress = {
            id: $('#editId').val(),
            firstName: $('#editFirstName').val(),
            lastName: $('#editLastName').val(),
            streetAddress: $('#editStreetAddress').val(),
            city: $('#editCity').val().toUpperCase(),
            state: $('#editState').val(),
            zip: $('#editZip').val()

        };

        var myAddressData = JSON.stringify(myAddress);
        console.log(contextRoot);
      


        $.ajax({
            url: contextRoot + "/address/" + myAddress.id,
            type: "PUT",
            data: myAddressData,
            dataType: "json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");


            },
            success: function(data, status) {

                var tableRow = buildAddressRow(data);

                console.log(tableRow);
                console.log("yo your here");

                $('#address-row-' + data.id).replaceWith($(tableRow));

                $('#editAddressModal').modal('hide');


            },
            error: function(data, status) {
                var errors = data.responseJSON.errors;

                $.each(errors, function(index, error) {
                    $('#edit-address-validation-errors').append('<li>'+ error.message + '</li>');

                });


            }


        });

    });
    
    
     $('#editAddressModal').on('shown.bs.modal', function (e) { 
         
           var link = $(e.relatedTarget);
        var addressId = link.data('address-id');
        
        $.ajax({
            url: contextRoot + "/address/" + addressId,
            type: "GET",
            dataType: "json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                $('#editId').val(data.id);
             $('#editFirstName').val(data.firstName);
              $('#editLastName').val(data.lastName);
               $('#editStreetAddress').val(data.streetAddress);
                $('#editCity').val(data.city);
                 $('#editState').val(data.state);
                 $('#editZip').val(data.zip);
                
            },
            error: function(data, status) {


                
            }

            
        });
    
     });    

    function buildAddressRow(data ) {

        var tableRow = '\
                    <tr id="address-row-' + data.id +'"> \n\
                    <td>' + data.firstName + ' ' + data.lastName + '</td> \n\
                    <td>' + data.streetAddress + '</td> \n\
                     <td>' + data.city + '</td> \n\
                      <td>' + data.state + '</td> \n\
                       <td>' + data.zip + '</td> \n\
                    <td><a data-address-id="' + data.id + '" data-toggle="modal" data-target="#editAddressModal">Edit</a></td> \n\
                    <td><a data-address-id="' + data.id + '" data-toggle="modal" data-target="#deleteAddressModal">Delete</a></td> \n\
                    </tr>';

        return tableRow;

    }




     

    $('#deleteAddressButton').on('click', function(e) {


        e.preventDefault();
        


        var myAddress = {
            id: $('#deleteId').val(),
            firstName: $('#deleteFirstName').text(),
            lastName: $('#deleteLastName').text(),
            streetAddress: $('#deleteStreetAddress').text(),
            city: $('#deleteCity').text(),
            state: $('#deleteState').text(),
            zip: $('#deleteZip').text()

        };

        var myAddressData = JSON.stringify(myAddress);
        console.log(contextRoot);
      


        $.ajax({
            url: contextRoot + "/address/" + myAddress.id,
            type: "DELETE",
            data: myAddressData,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");


            },
            success: function(data, status) {

                 $('#address-row-' + myAddress.id).remove();
                $('#deleteAddressModal').modal('hide');

            },
            error: function(data, status) {
                console.log("error");

            }


        });

    });

    $('#deleteAddressModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var addressId = link.data('address-id');

        $.ajax({
            url: contextRoot + "/address/" + addressId,
            type: "GET",
            dataType: "json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                $('#deleteId').val(data.id);
                $('#deleteFirstName').text(data.firstName);
                $('#deleteLastName').text(data.lastName);
                $('#deleteStreetAddress').text(data.streetAddress);
                $('#deleteCity').text(data.city);
                $('#deleteState').text(data.state);
                $('#deleteZip').text(data.zip);


            },
            error: function(data, status) {
                alert("address not found");


            }


        });

    });

  });