<%-- 
    Document   : home
    Created on : Sep 15, 2016, 11:08:17 AM
    Author     : mymac
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      
         <jsp:include page="navbar.jsp"/>
        <title>Home</title>
    </head>
    <body>
                  <div class="col-sm-6"  >
           <h2>Address List</h2>
            <table class="table table-responsive" id="addressTable5" >
        
 
                <tr>
                    <th>Name</th>
                    <th>Street Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Zip</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr> 
                <c:forEach items="${addressList}" var="address">
                    <tr id="address-row-${address.id}">
                        <td>${address.firstName} ${address.lastName}</td>
                        <td>${address.streetAddress}</td>
                        <td>${address.city}</td>
                        <td>${address.state}</td>
                        <td>${address.zip}</td>
                        <td><a data-address-id="${address.id}" data-toggle="modal" data-target="#editAddressModal">Edit</a></td>
                        <td><a data-address-id="${address.id}" data-toggle="modal" data-target="#deleteAddressModal">Delete</a></td>
                    </tr>
                </c:forEach>
            </table>           
        </div>
         
        
        <div class="col-sm-6 ">
            <h4>Add New Address</h4>
            
            <form method="POST" commandName="address" action="${pageContext.request.contextPath}/address/create" class="form-horizontal" >
                <div class="form-group">
                    <label for="inputFirstName3" class="col-sm-2 control-label">First Name:</label>
                    <div class="col-sm-10">
                        <input type="text" path="firstName" class="form-control" id="inputFirstName3" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputFirstName3" class="col-sm-2 control-label">Last Name:</label>
                    <div class="col-sm-10">
                        <input type="text" path="lastName" class="form-control" id="inputLastName3" />
                    </div>
                </div>                
                <div class="form-group">
                    <label for="inputStreetAddress3" class="col-sm-2 control-label">Street Address:</label>
                    <div class="col-sm-10">
                        <input type="text" path="streetAddress" class="form-control" id="inputStreetAddress3" />
                    </div>
                </div>  
                <div class="form-group">
                    <label for="inputCity3" class="col-sm-2 control-label"> City:</label>
                    <div class="col-sm-10">
                        <input type="text" path="city"  class="form-control" id="inputCity3" />
                    </div>
                </div>          
                <div class="form-group">
                <label for="state" class="col-sm-2 control-label">State</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputState3" name="state">
                        <option value="">N/A</option>
                        <option value="AK">Alaska</option>
                        <option value="AL">Alabama</option>
                        <option value="AR">Arkansas</option>
                        <option value="AZ">Arizona</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DC">District of Columbia</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="IA">Iowa</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MD">Maryland</option>
                        <option value="ME">Maine</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MO">Missouri</option>
                        <option value="MS">Mississippi</option>
                        <option value="MT">Montana</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="NE">Nebraska</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NV">Nevada</option>
                        <option value="NY">New York</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VA">Virginia</option>
                        <option value="VT">Vermont</option>
                        <option value="WA">Washington</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WV">West Virginia</option>
                        <option value="WY">Wyoming</option>
                        </select>
                </div>
                </div>

                <div class="form-group">
                    <label for="inputZip3" class="col-sm-2 control-label"> Zip:</label>
                    <div class="col-sm-10">
                        <input type="number" path="zip"  class="form-control" id="inputZip3" />
                    </div>
                </div>
                <strong><div id="create-address-validation-errors"  style="color:red;"> </div></strong>

                  <div class="form-group">
                     <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default" value="Save" id="addAddressButton">Add</button>
                     </div>
                  </div>
       
            </form>
            
        </div>
                  <!-- ------------------------------Modal for edit--------------------------------------------------------- -->

                  <div class="modal fade" id="editAddressModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit Address Info</h4>
                              </div>
                              <div class="modal-body">
                                  <input type="hidden" id="editId" />
                                  <table class="table table-striped">
                                          <tr>
                                          <th>First Name:</th>
                                          <td> <input type="text" path="firstName" class="form-control" id="editFirstName" /></td>
                                      </tr>
                                     
                            
                                         <tr>
                                          <th>Last Name:</th>
                                          <td> <input type="text" path="firstName" class="form-control" id="editLastName" /></td>
                                      </tr>                                                
                                         <tr>
                                          <th>Street Address:</th>
                                          <td> <input type="text" path="firstName" class="form-control" id="editStreetAddress" /></td>
                                      </tr> 
    
                                      <tr>
                                          <th>City:</th>
                                          <td> <input type="text" path="firstName" class="form-control" id="editCity" /></td>
                                      </tr> 
                                     <tr>
                                          <th>Zip:</th>
                                          <td> <input type="number" path="firstName" class="form-control" id="editZip" /></td>
                                      </tr>
                                      <tr>
                                <div>
                                        <th></th>
                            
                                    <select class="form-control" id="editState" name="state" >
                                              <option value="">N/A</option>
                                              <option value="AK">Alaska</option>
                                              <option value="AL">Alabama</option>
                                              <option value="AR">Arkansas</option>
                                              <option value="AZ">Arizona</option>
                                              <option value="CA">California</option>
                                              <option value="CO">Colorado</option>
                                              <option value="CT">Connecticut</option>
                                              <option value="DC">District of Columbia</option>
                                              <option value="DE">Delaware</option>
                                              <option value="FL">Florida</option>
                                              <option value="GA">Georgia</option>
                                              <option value="HI">Hawaii</option>
                                              <option value="IA">Iowa</option>
                                              <option value="ID">Idaho</option>
                                              <option value="IL">Illinois</option>
                                              <option value="IN">Indiana</option>
                                              <option value="KS">Kansas</option>
                                              <option value="KY">Kentucky</option>
                                              <option value="LA">Louisiana</option>
                                              <option value="MA">Massachusetts</option>
                                              <option value="MD">Maryland</option>
                                              <option value="ME">Maine</option>
                                              <option value="MI">Michigan</option>
                                              <option value="MN">Minnesota</option>
                                              <option value="MO">Missouri</option>
                                              <option value="MS">Mississippi</option>
                                              <option value="MT">Montana</option>
                                              <option value="NC">North Carolina</option>
                                              <option value="ND">North Dakota</option>
                                              <option value="NE">Nebraska</option>
                                              <option value="NH">New Hampshire</option>
                                              <option value="NJ">New Jersey</option>
                                              <option value="NM">New Mexico</option>
                                              <option value="NV">Nevada</option>
                                              <option value="NY">New York</option>
                                              <option value="OH">Ohio</option>
                                              <option value="OK">Oklahoma</option>
                                              <option value="OR">Oregon</option>
                                              <option value="PA">Pennsylvania</option>
                                              <option value="PR">Puerto Rico</option>
                                              <option value="RI">Rhode Island</option>
                                              <option value="SC">South Carolina</option>
                                              <option value="SD">South Dakota</option>
                                              <option value="TN">Tennessee</option>
                                              <option value="TX">Texas</option>
                                              <option value="UT">Utah</option>
                                              <option value="VA">Virginia</option>
                                              <option value="VT">Vermont</option>
                                              <option value="WA">Washington</option>
                                              <option value="WI">Wisconsin</option>
                                              <option value="WV">West Virginia</option>
                                              <option value="WY">Wyoming</option>
                                              </select>
                                      </tr>
                                      </div>
                                     </div>
                                  </table>
                              </div>
                              <div class="modal-footer">
                                  <strong><div id="edit-address-validation-errors"  style="color:red;"> </div></strong>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary" id="editAddressButton">Save changes</button>
                              </div>
                          </div>
                      </div>
                  </div>

                  <!-- ------------------------------Modal for delete--------------------------------------------------------- -->

                  <div class="modal fade" id="deleteAddressModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Delete Address</h4>
                              </div>
                              <div class="modal-body">
                                    <input type="hidden"id="deleteId" />
                                  <table class="table table-striped">
                                      <tr>
                                          <th>First Name:</th>
                                          <td id="deleteFirstName"></td>
                                      </tr>
                                      <tr>
                                          <th>Last Name:</th>
                                          <td id="deleteLastName"></td>
                                      </tr>
                                      <tr>
                                          <th>Street Address:</th>
                                          <td id="deleteStreetAddress"></td>
                                      </tr>
                                      <tr>
                                          <th>City:</th>
                                          <td id="deleteCity"></td>
                                      </tr>
                                      <tr>
                                          <th>State:</th>
                                          <td id="deleteState"></td>
                                      </tr>
                                      <tr>
                                          <th>Zip:</th>
                                          <td id="deleteZip"></td>
                                      </tr>
                                  </table>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary" id="deleteAddressButton">Delete</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  <script>
                      var contextRoot = "${pageContext.request.contextPath}";
                  </script>
                  <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js" ></script>
                  <script src="${pageContext.request.contextPath}/js/app.js" ></script>
                  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
