<%-- 
    Document   : edit
    Created on : Sep 14, 2016, 9:24:00 AM
    Author     : mymac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <jsp:include page="navbar.jsp"/>
        <title>Edit Page</title>
    </head>
    <body>
        <h1>Edit Contact</h1>
        <!--this takes place of the project name, if i wanna change the project name  -->                
             
                <div class="container">
                    <h3><center>Edit Address</center></h3>
                       <form action="${pageContext.request.contextPath}/address/edit" method="POST">
                          <input type="hidden" name="id" value="${address.id}" /> 
                        <div class="form-group">
                          <label for="firstName">Title: </label>
                          <input type="text" class="form-control" name="firstName" value="${address.firstName}">
                        </div>
                        <div class="form-group">
                          <label for="lastName">Director: </label>
                          <input type="text" class="form-control" name="lastName" value="${address.lastName}">
                        </div>
                        <div class="form-group">
                          <label for="streetAddress">Rating: </label>
                          <input type="text" class="form-control" name="streetAddress" value="${address.streetAddress}">
                        </div>                  
                        <div class="form-group">
                          <label for="city">Studio: </label>
                          <input type="text" class="form-control" name="city" value="${address.city}">
                        </div>
                        <div class="form-group">
                          <label for="state">User Rating: </label>
                          <input type="text" class="form-control" name="state" value="${address.state}">
                        </div>   
                        <div class="form-group">
                          <label for="zip">Release Year: </label>
                          <input type="text" class="form-control" name="zip" value="${address.zip}">
                        </div>    
                                     
                        <button type="submit" class="btn btn-default">Edit</button>
                      </form>
                </div>
   
        
    </body>
    
</html>
