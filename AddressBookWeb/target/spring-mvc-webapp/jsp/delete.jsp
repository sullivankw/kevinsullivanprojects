<%-- 
    Document   : delete
    Created on : Sep 14, 2016, 11:20:36 AM
    Author     : mymac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <jsp:include page="navbar.jsp"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
         <h1>Delete Contact</h1>

                    <form action="${pageContext.request.contextPath}/address/delete" method="POST">
            
                <input type="hidden" name="id" value="${address.id}" />    
                        <table class="table table-striped">
            <tr>
                <th>Name</th>
                <th>Street Address</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
            </tr>
            <tr>
                <td>${address.firstName} ${address.lastName}</td>
                <td>${address.streetAddress}</td>
                <td>${address.city}</td>
                <td>${address.state}</td>
                <td>${address.zip} </td>
            </tr>
            
        </table>
            <button type="submit" class="btn btn-danger">Delete</button>
                    </form>   
        </div>                 
    </body>
</html>
