<%-- 
    Document   : show
    Created on : Sep 14, 2016, 10:22:26 AM
    Author     : mymac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <jsp:include page="navbar.jsp"/>
        <title>Details</title>
    </head>
    <body>
        <div class="container">
        <h2><center>Updated Address</center></h2>
        <table class="table table-striped">
            <tr>
                <th>Name</th>
                <th>Street Address</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
            </tr>
            <tr>
                <td>${address.firstName} ${address.lastName} </td>
                <td>${address.streetAddress}</td>
                <td>${address.city}</td>
                <td>${address.state}</td>
                <td>${address.zip} </td>
    
            </tr>
        </table>
        </div>


    </body>
</html>
