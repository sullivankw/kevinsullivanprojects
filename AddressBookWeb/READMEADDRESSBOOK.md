To launch the program...

Select the downloads link from the nagigation controls to download a zip of this repo. In the AddressBookWeb folder all the files necessary to
launch this web app are included. However, a local server will need to be configured on your local machine to launch the app. The program was created using Intellij
IDEA and the open-source web server Apache Tomcat.

A link to walk you through Tomcat setup on your local machine: https://tomcat.apache.org/tomcat-7.0-doc/setup.html

The testing environment is setup to use the data from addressbookweb.testaddresslist.sql which is included in the project folder.

**************************************************************************************************************************************************

Address Book Web is a Spring MVC web app designed to manage personal addresses.

Features:

A. Tables and forms were created on the home page using bootstrap.
B. Javascript is used to create event listeners and display error messages.
C. AJAX used to to create JSON objects in order to pass user input to the controller.
D. A mySQL table was created fortable for testing and actual data storage. 
E. The addressbookweb.addresslist table contains id, street_address, first_name, last_name, city,
state, and zip columns.
F. Server side validation implemeted using Spring and custom error messages created using Hibernate


