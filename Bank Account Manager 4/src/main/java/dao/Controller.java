package dao;

import com.mycompany.ui.theconsoleio.EnhancedConsoleIO;

/**
 * Created by mymac on 9/7/16.
 */
public class Controller {

    protected int pin = 5555;
    private boolean playAgain = true;

    private EnhancedConsoleIO io = new EnhancedConsoleIO();
    private Checking checking = new Checking();
    private Savings savings = new Savings();

    public void run() {

        enterPin();

        while (playAgain)
        {
            io.displayString("Please choose an account. ");
        int accountChoice = io.getIntRange("1. Checking Account 2. Savings Account 3. Exit ", 3, 1);

        switch (accountChoice) {

            case 1:
                checkingPath();
                break;
            case 2:
                savingsPath();
                break;
            case 3:
                playAgain = false;

        }

    }

    }

    public void enterPin() {
        boolean go = false;
        while (!go) {
            int pinChoice = io.getIntRange("Please enter your 4 digit PIN", 9999, 1000);
            if (pinChoice == pin) {
                go = true;
            } else {
                io.displayString("Incorrect match");
            }
        }


    }

        public void checkingPath() {
            boolean playAgainAgain = true;

            while (playAgainAgain) {

                int accountChoice = toWhereTheMoneyIsGoing();

                switch (accountChoice) {
                    case 1:
                        checking.deposit();
                        checking.viewBalance();
                        break;
                    case 2:
                        checking.withdraw();
                        checking.viewBalance();
                        break;
                    case 3:
                        checking.viewBalance();
                        break;
                    case 4:
                        playAgainAgain = false;
                        break;

                }
            }
        }


    public void savingsPath() {

        boolean playAgainAgain = true;

        while (playAgainAgain) {

            int accountChoice = toWhereTheMoneyIsGoing();

            switch (accountChoice) {
                case 1:
                    savings.deposit();
                    savings.viewBalance();
                    io.displayString("\n");
                    break;
                case 2:
                    savings.withdraw();
                    savings.viewBalance();
                    io.displayString("\n");
                    break;
                case 3:
                    savings.viewBalance();
                    break;
                case 4:
                    playAgainAgain = false;
                    break;

            }

        }

    }

    public int toWhereTheMoneyIsGoing () {

                io.displayString("Please choose from the following options. ");
                int accountChoice = io.getIntRange("1. Deposit 2. Withdraw 3. View Balance 4. Exit ", 4, 1);

                return accountChoice;

        }


}
