/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dto.Audit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
//import com.thesoftwareguild.dto.Audit;

/**
 *
 * @author mymac
 */
public class AuditDao {
    
    private List<Audit> audits;
    
    private static final String TOKEN = ",";
    
    Audit auditMan = new Audit();

    public Audit add(Audit audit) {

        audits = decode();

        audit.setAuditNumber(audits.size() + 1);

        audits.add(audit);

        encode();

        return audit;

    }


    public void encode() {

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter("Audits.txt"));

            for (Audit s : audits) {

                out.print(s.getAuditNumber());
                out.print(TOKEN);

                out.print(s.getOperation());
                out.print(TOKEN);

                out.print(s.getDate());
                out.print(TOKEN);
                
                out.print(s.getTime());
                out.print(TOKEN);

//                out.print(s.getAmountAtOperation());;
//                out.print("\n");


            }

            out.flush();


        } catch (IOException ex) {


        } finally {

            out.close();
        }

    }

    public List<Audit> decode() {

        List<Audit> tempAuditList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("Audits.txt")));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);

                Audit a = new Audit();
                a.setAuditNumber(Integer.parseInt(stringParts[0]));
                a.setOperation(stringParts[1]);
                a.setDate(stringParts[2]);
                a.setTime(stringParts[3]);
                //a.setAmountAtOperation(Double.parseDouble(stringParts[4]));

                tempAuditList.add(a);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(AuditDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tempAuditList;
    }


    public void createTextFile(File file) {


        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(file));

            out.flush();

        } catch (IOException ex) {

        } finally {

            out.close();
        }

    }
}
