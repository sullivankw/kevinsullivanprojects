package dao;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mycompany.ui.theconsoleio.EnhancedConsoleIO;
import dto.CheckingAccount;

/**
 * Created by mymac on 9/7/16.
 */
public class Checking extends Accounts {

    private EnhancedConsoleIO io = new EnhancedConsoleIO();
    private List<CheckingAccount> checkings;
    private static final String TOKEN = ",";


    public Checking() {
        checkings = decode();

    }

    public void encode() {

        PrintWriter out = null;

        try{
            out = new PrintWriter(new FileWriter("Checking.txt"));

            for(CheckingAccount s : checkings)
            {
                out.print(s.getId());
                out.print(TOKEN);

                out.print(balance);
                out.print(TOKEN);

                out.print(pendingBalance);
                out.print("\n");
            }
            out.flush();

        } catch (IOException ex) {

        } finally {

            out.close();
        }

    }

    public List<CheckingAccount> decode() {

        List<CheckingAccount> tempCheckingAccountList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("Checking.txt")));

            while(sc.hasNextLine() ) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);

                CheckingAccount ca = new CheckingAccount();
                ca.setId(Integer.parseInt(stringParts[0]));
                balance = (Double.parseDouble(stringParts[1]));
                pendingBalance = (Double.parseDouble(stringParts[2]));

                tempCheckingAccountList.add(ca);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Checking.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tempCheckingAccountList;
    }

    public void deposit() {

        Double oldBalance = balance;
        String confirm = "";

        Double money = io.getDoubleRange("Enter the amount to deposit. ", 1000000000, 1);
        if(money >=10000) {
           confirm =  io.getString("Deposits over 10000 must be approved by a manager. If you continue the money will be pending and not available until approved. Continue? y/n ");
            if (confirm.equals("y") || confirm.equals("Y")) {
                pendingBalance = (pendingBalance + money);
            } else {
                io.displayString("Action cancelled. ");
            }
        } else {
            balance = (oldBalance + money);
        }

        encode();

    }

    public void withdraw() {
        io.displayString("Your balance is: " + balance);
        Double oldBalance = balance;
        String confirm = "";

            Double money = io.getDoubleRange("Enter the amount to withdraw. ", oldBalance + 100, 1);
            if(money > oldBalance ) {
                confirm =  io.getString("You will overdraw your account. A $10 dollar fee will be added. Do you wish to continue? y/n ");
                  if (confirm.equals("y") || confirm.equals("Y")) {
                    balance = (oldBalance - money - 10.00);
                  } else {
                      io.displayString("Action cancelled. ");
                  }
            } else {
                balance = (oldBalance - money);
            }

        encode();

    }

    public void viewBalance() {

        io.displayString("Your balance is: " + balance);
        io.displayString("Your pending balance is: " + pendingBalance );
        io.displayString("--------------------------------------------");

    }



}
