package dao;

import com.mycompany.ui.theconsoleio.EnhancedConsoleIO;
import dto.SavingsAccount;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by mymac on 9/7/16.
 */
public class Savings extends Accounts {

    private EnhancedConsoleIO io = new EnhancedConsoleIO();
    private List<SavingsAccount> savings;
    private static final String TOKEN = ",";


    public Savings() {
        savings = decode();

    }

    public void encode() {

        PrintWriter out = null;

        try{
            out = new PrintWriter(new FileWriter("Savings.txt"));

            for(SavingsAccount s : savings)
            {
                out.print(s.getId());
                out.print(TOKEN);

                out.print(balance);
                out.print("\n");
            }
            out.flush();

        } catch (IOException ex) {

        } finally {

            out.close();
        }

    }

    public List<SavingsAccount> decode() {

        List<SavingsAccount> tempSavingsAccountList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("Savings.txt")));

            while(sc.hasNextLine() ) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);

                SavingsAccount ca = new SavingsAccount();
                ca.setId(Integer.parseInt(stringParts[0]));
                balance = (Double.parseDouble(stringParts[1]));

                tempSavingsAccountList.add(ca);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Savings.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tempSavingsAccountList;
    }

    public void deposit() {

        Double oldBalance = balance;

        Double money = io.getDoubleRange("Enter the amount to deposit. ", 1000000, 1);
        balance = oldBalance + money;

        encode();

    }

    public void withdraw() {

        io.displayString("Your balance is: " + balance);
        Double oldBalance = balance;

        Double money = io.getDoubleRange("Enter the amount to withdraw. ", oldBalance, 1);
        balance = oldBalance - money;

        encode();

    }

    public void viewBalance() {

        io.displayString("Your balance is: " + balance);
        io.displayString("--------------------------------------------");

    }



}
