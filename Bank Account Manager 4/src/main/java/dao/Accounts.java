package dao;

import com.mycompany.ui.theconsoleio.EnhancedConsoleIO;
//import com.sun.tools.javac.util.List;
import dto.SavingsAccount;

/**
 * Created by mymac on 9/7/16.
 */
public abstract class  Accounts {

    protected Double balance;
    protected Double pendingBalance;


    abstract void deposit();

    abstract void withdraw();


    public void viewBalance() {

    }


    public void encode() {

    }

    //So, here my class has been declared abstract. so it's children must provide implementation
    //for the abstract methods
    //they do not have to provide implementation for the methods that are not declared abstract
    //however they can override them...this occurs at run time and can slow down progress

}
