package dto;

/**
 * Created by mymac on 9/13/16.
 */
public class Audit {

    int auditNumber;
    String operation;
    String date;
    String time;
    Double amountAtOperation;
    
    
    public Double getAmountAtOperation() {
        return amountAtOperation;
    }

    public void setAmountAtOperation(Double amountAtOperation) {
        this.amountAtOperation = amountAtOperation;
    }
    

    public int getAuditNumber() {
        return auditNumber;
    }

    public void setAuditNumber(int auditNumber) {
        this.auditNumber = auditNumber;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
