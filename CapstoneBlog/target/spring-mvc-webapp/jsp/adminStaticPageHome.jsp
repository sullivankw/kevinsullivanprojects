<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <jsp:include page="navbar.jsp"/>

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>

    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

</head>
    <body>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
    <div class="container">
        <table class="table table-striped" id="staticPageTable" >

            <tr>
                <th>Title</th>
                <th>Status</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
            <c:forEach items="${staticPageList}" var="pages">
                <tr id="staticpage-row-${pages.id}">
                    <td><a href="${pageContext.request.contextPath}/staticpage/show/${pages.id}">${pages.title}</a></td>
                    <td>${pages.active}</td>
                    <td><a href="${pageContext.request.contextPath}/staticpage/update/${pages.id}">Update</a></td>
                    <td><a data-staticpage-id="${pages.id}" data-toggle="modal" data-target="#deleteStaticPageModal">Delete</a></td>
                </tr>
            </c:forEach>
        </table>
    </div>

        <div class="container">
        <form class="form-horizontal" action="${pageContext.request.contextPath}/staticpage/" method="POST" id="form">

            <div class="col-sm-10">
                <%--<label for="addStaticTitle" class="col-sm-2 control-label">Static Page Title</label>--%>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="addStaticTitle" placeholder="Static Page Title">
                </div>
            </div>

            <div class="col-sm-10">
                <select class="form-control" id="staticactive" path="active">
                    <option>Active</option>
                    <option>Inactive</option>
                </select>
            </div>

            <div class="col-sm-10">
                <textarea id="mytextarea" class="tinymce" rows="1" cols="10" >Body</textarea>
            </div>
            <div class="col-sm-10">
                <input type="button" id="CreateStaticPageButton" value="Add Static Page"/>
            </div>


        </form>
        </div>

    <%--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Delete Post Modal!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!--%>

    </div>

    <div class="modal fade" id="deleteStaticPageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Static Page</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="deleteId" />

                    <table>
                        <tr>
                            <th>Title: </th>
                            <td id="deleteTitle" ></td>
                        </tr>
                        <tr>
                            <th>Currently Active: </th>
                            <td id="deleteActive" ></td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="deleteStaticPageButton">Delete Static Page</button>
                </div>
            </div>
        </div>
    </div>
    </sec:authorize>
</body>



<script>
    var contextRoot = "${pageContext.request.contextPath}";
</script>

<%--<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>--%>

<script src="${pageContext.request.contextPath}/js/addStaticPage.js"></script>

<script src="${pageContext.request.contextPath}/js/deleteStaticPage.js"></script>

<%--<script src="${pageContext.request.contextPath}/plugin/tinymce/plugins/justboil.me-master/editor_plugin.js"></script>--%>

<%--<script src="${pageContext.request.contextPath}/plugin/tinymce/plugins/justboil.me-master/editor_plugin_src.js"></script>--%>

<%--<script src="${pageContext.request.contextPath}/plugin/plugins/justboil.me-master/plugin.js"></script>--%>

<%--<script src="${pageContext.request.contextPath}/plugin/tinymce/plugins/jbimages/plugin.min.js"></script>--%>

<script type="text/javascript" src="${pageContext.request.contextPath}/plugin/tinymce/jquery.tinymce.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/plugin/tinymce/tinymce.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/plugin/tinymce/init-tinymce.js"></script>

<%--<script type="text/javascript" src="${pageContext.request.contextPath}/js/init-tinymce.js"></script>--%>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

</body>
</html>
