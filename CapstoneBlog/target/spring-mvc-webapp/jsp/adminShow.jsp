<%-- 
    Document   : adminShow
    Created on : Oct 8, 2016, 3:28:19 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="navbar.jsp"/>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>

        <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

        <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    </head>
    <body>

        <h1>Blog Post Entry</h1>


                <table class="container">
                    <tr>
                        <th>Title:</th>
                        <td>${blog.title}</td>
                    </tr>
                    <tr>
                        <th>Body:</th>
                        <td>${blog.body}</td>
                    </tr>
                    <tr>
                        <th>Approval:</th>
                        <td>${blog.approvalStatus}</td>
                    </tr>
                                          
                </table>

    </body>
</html>
