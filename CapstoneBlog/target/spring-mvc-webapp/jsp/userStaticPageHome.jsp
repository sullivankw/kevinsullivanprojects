
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <jsp:include page="navbar.jsp"/>

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>

    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


</head>
<body>

<div class="container">
    <table class="table table-responsive" id="staticPageTable" >
        <tr>
            <th></th>
            <th></th>
        </tr>
        <c:forEach items="${staticPageList}" var="pages">
            <tr id="staticpage-row-${pages.id}">
                <td><a href="${pageContext.request.contextPath}/staticpage/show/${pages.id}">${pages.title}</a></td>
                <td>Pages: ${pages.body}</td>
            </tr>
        </c:forEach>
    </table>
</div>

<script>
    var contextRoot = "${pageContext.request.contextPath}";
</script>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>