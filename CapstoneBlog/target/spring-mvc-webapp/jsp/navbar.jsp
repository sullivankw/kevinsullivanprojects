<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <hr>
    <style>
        ul {
            list-style-type: none;
            background-color: #f1f1f1;
        }

        .center-block {
            /*display: block;*/
            width: 40%;
            height: 20%;
        }

    </style>

    <title>Bowling Blog!</title>
        <div class="container">
        <div class="center-block">
        <center><img src="${pageContext.request.contextPath}/images/wide-bowling-shot.jpg" alt="Staff" id="photo1" class="img-responsive"></center>
        </div>
        </div>
</head>
<body>

    <h2 class="companyName"><center>Bobby's Bowling Blog</center></h2>
    <div class="container" >
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="${pageContext.request.contextPath}"><span>Home</span></a></li>
            <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
                <li role="presentation"><a href="${pageContext.request.contextPath}/adminhome"><span>Admin Home</span></a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/staticpage"><span>Edit Static Pages</span></a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/marketerhome"><span>Marketer Home</span></a></li>
            </sec:authorize>
            <li role="presentation">
                <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
            </li> 
            <c:forEach items="${stashiesList}" var="stashies">
                <li role="presentation"><a href="${pageContext.request.contextPath}/staticpage/show/${stashies.id}"><span>${stashies.title}</span></a></li>
                        </c:forEach>
        </ul>

    </div>


</body>

</html>