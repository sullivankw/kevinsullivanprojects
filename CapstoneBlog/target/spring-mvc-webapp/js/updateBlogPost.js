$(document).ready(function() {


    function buildBlogPostRow(data) {

        var newPublishDate = new Date(data.publishDate);

        newPublishDate = newPublishDate.getFullYear() + '-' + (newPublishDate.getMonth() + 1) + '-' + (newPublishDate.getDate() + 1);

        var newExpirationDate = new Date(data.expirationDate);

        newExpirationDate = newExpirationDate.getFullYear() + '-' + (newExpirationDate.getMonth() + 1) + '-' + (newExpirationDate.getDate() + 1);


        var tableRow = '\
                        <tr id="blogpost-row-' + data.id + '"> \n\
                            <td>' + data.title + '</td> \n\
                            <td>' + newPublishDate + '</td> \n\
                            <td>' + newExpirationDate + '</td> \n\
                            <td><a href="/CapstoneBlog/blogpost/update/' + data.id +'">Update</a></td> \n\
                            <td><aa href="/CapstoneBlog/blogpost/delete/' + data.id +'"Delete</a></td> \n\
                        </tr>';

        return tableRow;
    }

    window.onload = function(){

        var btn = document.getElementById('updateBlogPostSubmitButton');

        btn.onclick = function(){

            tinymce.triggerSave();

            $('#update-blogpost-validation-errors').text('');

            $('#confirmationUpdate').text('');

            var myBlogPost = {
                id: $('#updateId').val(),
                title: $('#updateTitle').val(),
                body: $('#mytextarea').val(),
                approvalStatus: $('#updateApproval').val(),
                publishDate: $('#updatePublishDate').val(),
                expirationDate: $('#updateExpirationDate').val(),
                category: $('#updateCategories').val()

            };

            console.log(myBlogPost);

            var myBlogPostData = JSON.stringify(myBlogPost);

            $.ajax({
                url: contextRoot + "/blogpost/update/" + myBlogPost.id,
                type: "PUT",
                data: myBlogPostData,
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-type", "application/json");

                },
                success: function(data, status) {

                    var tableRow = buildBlogPostRow(data);

                    $('#blogPost-row-' + data.id).replaceWith($(tableRow));

                    $('#confirmationUpdate').append("Your updates have been saved.");

                    var div = document.createElement('div');
                    div.setAttribute('class', 'someClass');
                    div.innerHTML = document.getElementById('homeRedirect').innerHTML;
                    document.getElementById('targetElement').appendChild(div);

                    console.log(myBlogPostData);


                },
                error: function(data, status) {

                    // var errors = data.responseJSON.errors;
                    //
                    // $.each(errors, function(index, error) {
                    //     $('#update-blogpost-validation-errors').append('<li>'+ error.message + '</li>');
                    //
                    // });

                    console.log("An error occurred while submitting updates.");

                }

            })

        }
    }

});