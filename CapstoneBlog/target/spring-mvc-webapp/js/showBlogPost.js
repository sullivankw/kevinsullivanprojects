/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    window.onload = function () {

        var link = $(e.relatedTarget);

        var myBlogPost = {
            id: $('#showId').text()
//                title: $('#showTitle').text(),
//                body: $('#myTextArea').text(),
//                approvalStatus: $('#showApprovalStatus').text(),
//                authorId: $('#showAuthorId').text(),
//                publishDate: $('#showPublishDate').text(),
//                expirationDate: $('#showExpirationDate').text()

        };
        var myBlogPostData = JSON.stringify(myBlogPost);

        $.ajax({
            url: contextRoot + "/blogpost/" + myBlogPost.id,
            type: "GET",
            data: myBlogPostData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {

                var tableRow = buildTableRow(data);

                $('#showTitle').text(data.title);
                $('#showPage').append(data.body);
               
                $('#blogPost-row-' + data.id).replaceWith($(tableRow));
            },
            error: function (data, status) {
                alert("BLOG POST NOT FOUND");
            }
        });

    };


});