/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#deleteBlogPostModal').on('shown.bs.modal', function (e) {
        var link = $(e.relatedTarget);

        var blogPostId = link.data('blogpost-id');
        console.log(blogPostId);

        $.ajax({
            url: contextRoot + "/blogpost/" + blogPostId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json")
            },
            success: function (data, status) {
                $('#deleteId').val(data.id);
                $('#deleteTitle').text(data.title);
                $('#deleteBody').text(data.body);
                $('#deleteApprovalStatus').text(data.approvalStatus);
                $('#deleteAuthorId').text(data.authorId);
                $('#deletePublishDate').text(data.publishDate);
                $('#deleteExpirationDate').text(data.expirationDate);
            },
            error: function (data, status) {
                alert("BLOG POST NOT FOUND");
            }
        });
    });
    $('#deleteBlogPostButton').on('click', function (e) {

        e.preventDefault();

        var myBlogPost = {
            id: $('#deleteId').val(),
            title: $('#deleteTitle').text,
            body: $('#deleteBody').text(),
            approval_status: $('#deleteApprovalStatus').text(),
            author_id: $('#deleteAuthorId').text(),
            publish_date: $('#deletePublishDate').text(),
            expiration_date: $('#deleteExpirationDate').text()
            
        };
        console.log(myBlogPost.id);

        var myBlogPostData = JSON.stringify(myBlogPost);
        console.log(contextRoot);


        $.ajax({
            url: contextRoot + "/blogpost/" + myBlogPost.id,
            type: "DELETE",
            data: myBlogPostData,
//            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                // var tableRow = buildContactRow(data);
                console.log(myBlogPost.id);
                
                $('#blogpost-row-' + myBlogPost.id).remove();

                $('#blogpost-market-row-' + myBlogPost.id).remove();

                $('#deleteBlogPostModal').modal('hide');

            },
            error: function (data, status) {
                console.log("ERROR");
            }
        });
    });
    
});

