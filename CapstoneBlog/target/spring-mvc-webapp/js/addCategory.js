/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#addCategoryButton').on('click', function (e) {

        e.preventDefault();

        var theCategory = {
            categoryName: $('#newCategory').val()

        };

        console.log(theCategory.categoryName);

        var categoryData = JSON.stringify(theCategory);


        $.ajax({
            url: contextRoot + "/blogpost/category",
            type: "POST",
            data: categoryData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                var newOption = $('<option value="'+data.categoryName+'">'+data.categoryName+'</option>');
                $('#categorySelect').append(newOption);
                $("#categorySelect").trigger("chosen:updated");

            },
            error: function (data, status) {



            }

        });

    });
});