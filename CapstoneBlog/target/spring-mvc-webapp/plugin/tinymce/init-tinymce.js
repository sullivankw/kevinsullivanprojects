// $(document).ready(function() {

tinymce.init({
    selector: "textarea.tinymce",
    height: 400,
    theme: 'modern',
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools save image,paste'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    toolbar2: 'print preview media | forecolor backcolor emoticons | save image,paste',
    image_advtab: true,
    paste_data_images: true,
    templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
    ]
});

// tinymce.init({
//     selector: 'textarea.tinymce',
//     height: 500,
//     plugins: [
//         "advlist autolink lists link image charmap print preview anchor",
//         "searchreplace visualblocks code fullscreen",
//         "insertdatetime media table contextmenu paste imagetools"
//     ],
//     toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image paste",
//     imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
//     paste_data_images: true,
//     content_css: [
//         '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
//         '//www.tinymce.com/css/codepen.min.css'
//     ]
// });
//
// tinymce.init({
//     selector: "textarea.tinymce",  // change this value according to your HTML
//     plugins: "image",
//     menubar: "insert",
//     toolbar: "image",
//     image_list: [
//         {title: 'My image 1', value: '/Users/mymac/Desktop/bowlingsketch.jpeg'},
//         {title: 'My image 2', value: 'http://www.moxiecode.com/my2.gif'}
//     ]
// });



// });