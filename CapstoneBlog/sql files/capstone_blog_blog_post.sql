-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: capstone_blog
-- ------------------------------------------------------
-- Server version	5.7.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog_post`
--

DROP TABLE IF EXISTS `blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `body` varchar(1000) DEFAULT NULL,
  `approval_status` tinyint(4) DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_blog_post_1_idx` (`author_id`),
  CONSTRAINT `fk_blog_post_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_post`
--

LOCK TABLES `blog_post` WRITE;
/*!40000 ALTER TABLE `blog_post` DISABLE KEYS */;
INSERT INTO `blog_post` VALUES (140,'no','<p>Enter text here...</p>',0,NULL,'2016-10-19','2016-10-29'),(152,'last test','<p>Enter text here...ssss</p>\n<p>&nbsp;</p>\n<p>more animals</p>',0,NULL,'2016-10-06','2016-10-29'),(153,'hey man','<p>Enter text here...</p>',0,NULL,'2016-10-12','2016-10-31'),(154,'try 2','<p>Enter text here...ytr 2</p>',2,NULL,'2016-10-12','2016-10-17'),(156,'newly moved','<p>hguhiuhiuh</p>',0,NULL,'2016-10-12','2016-10-31'),(157,'This Post','<p>When you bowl.....sometimes you eat cheese.</p>\n<p>#cheese #bowlingwithcheese</p>\n<p>&nbsp;</p>',0,NULL,'2016-10-12','2016-10-31'),(159,'Check this out Admin','<h2>You don\'t pay enough....</h2>\n<p>#Iquit</p>',2,NULL,'2016-10-18','2016-10-31'),(160,'Lebowski vs Kingpin','<p>You know It!</p>',0,NULL,'2016-10-12','2016-10-29'),(161,'quick','<p>Enter text heressssnsjsjs</p>\n<p>&nbsp;</p>\n<p>#hhhhh</p>',2,NULL,'2016-10-12','2016-10-14');
/*!40000 ALTER TABLE `blog_post` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-21 21:51:45
