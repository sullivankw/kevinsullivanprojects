-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: capstone_blog
-- ------------------------------------------------------
-- Server version	5.7.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `static_page`
--

DROP TABLE IF EXISTS `static_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `body` varchar(1000) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `active` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_page`
--

LOCK TABLES `static_page` WRITE;
/*!40000 ALTER TABLE `static_page` DISABLE KEYS */;
INSERT INTO `static_page` VALUES (14,'<h2>Contact Us</h2>\n<p>At Bobby\'s Big Bad Bowling Balls we love to hear from our customers.</p>\n<p>You can reach us at:</p>\n<p>phone: 111-2222-333</p>\n<p>email: <a href=\"mailto:bobbysballs@gmail.com\">bobbysballs@gmail.com</a></p>\n<p>See you at Bobby\'s!</p>\n<p>&nbsp;</p>','Contact Us','Active'),(15,'<h2>Bobby\'s Staff:</h2>\n<p>John Bobby: John has been working here for a while.&nbsp;</p>\n<p>Contact John at JohnBobby@gmail.com</p>\n<p>Alan Bobby: Much like Bobby....Alan has been working here for a while.</p>\n<p>Contact John at AlanBobby@gmail.com</p>\n<p>Bruce Springsteen Bobby just started.</p>\n<p>Contact John at BruceSpringsteenBobby@gmail.com</p>\n<p>&nbsp;</p>','About Us','Active'),(16,'<p>You can reach us through the form below:</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>','Contact Form','Inactive'),(41,'<p>hi</p>','nnnn','Inactive');
/*!40000 ALTER TABLE `static_page` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-21 21:51:46
