//package com.mycompany.capstoneblog.dao;
//
//import com.mycompany.capstoneblog.dto.Category;
//import com.mycompany.capstoneblog.dto.User;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//import java.util.List;
//
//import static org.junit.Assert.*;
//import org.springframework.jdbc.core.JdbcTemplate;
//
///**
// * Created by mymac on 10/5/16.
// */
//public class UserDaoImplTest {
//
//    UserDao userDao;
//
//    User testA = new User();
//    User testB = new User();
//
//    public UserDaoImplTest() {
//
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("testspring-persistence.xml");
//        userDao = ctx.getBean("UserDao", UserDao.class);
//
//        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("DELETE FROM user");
//
//    }
//
//    @Before
//    public void setUp() throws Exception {
//        testA.setRole("admin");
//        testA.setPassword("Poop");
//        testA.setUserName("John");
//
//        testB.setRole("admin");
//        testB.setPassword("Poop");
//        testB.setUserName("John");
//        testB.setId(4);
//        //testB.setId(5);
//
//    }
//
//    @After
//    public void tearDown() throws Exception {
//
//    }
//
//    @Test
//    public void create() {
//
//        User c = userDao.create(testA);
//        Assert.assertEquals("admin", c.getRole());
//        Assert.assertEquals("Poop", c.getPassword());
//
//    }
////
//
//    @Test
//    public void read() {
//        
//        User test = userDao.create(testA);
//        
//        User t = userDao.read(test.getId());
//        
//        Assert.assertEquals(t.getPassword(), "Poop");
//
//    }
//
//    @Test
//    public void update() {
//
//        testB.setRole("testrole");
//        testB.setUserName("testusername");
//        testB.setPassword("testpassword");
//
//        userDao.update(testB);
//
//        Assert.assertEquals("testpassword", testB.getPassword());
//        Assert.assertEquals("testrole", testB.getRole());
//        Assert.assertEquals("testusername", testB.getUserName());
//
//    }
//
////    @Test
////    public void delete() throws Exception {
////        User j = userDao.read(3);
////        userDao.delete(j);
////
////
////    }
//    @Test
//    public void listAll() throws Exception {
//
//        userDao.create(testA);
//        userDao.create(testB);
//        
//        List<User> users = userDao.listAll();
//
//        Assert.assertEquals(users.size(), 2);
//
//    }
//
//}
