///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycompany.capstoneblog.dao;
//
//import com.mycompany.capstoneblog.dto.BlogPost;
//import com.mycompany.capstoneblog.dto.BlogPostHashTags;
//import com.mycompany.capstoneblog.dto.HashTag;
//import static java.nio.file.Files.list;
//import static java.rmi.Naming.list;
//import static java.util.Collections.list;
//import java.util.List;
//import junit.framework.Assert;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.jdbc.core.JdbcTemplate;
//
///**
// *
// * @author apprentice
// */
//public class BlogPostHashTagsDaoImplTest {
//
//    BlogPostHashTagsDao bphtDao;
//    BlogPostHashTags bphtTest = new BlogPostHashTags();
//
//    public BlogPostHashTagsDaoImplTest() {
//
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("testspring-persistence.xml");
//
//        bphtDao = (BlogPostHashTagsDao) ctx.getBean("BlogPostHashTagsDao");
//
//        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("DELETE FROM blog_hashtags");
//
//        JdbcTemplate cleaner2 = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("DELETE FROM blog_post");
//        
//        JdbcTemplate cleaner3 = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("DELETE FROM hash_tag");
//    }
//
//    @Before
//    public void setUp() {
//
//        bphtTest.setBlogId(3);
//        bphtTest.setHashTagId(2);
//
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of create method, of class BlogPostHashTagsDaoImpl.
//     */
//    @Test
//    public void testCreate() {
//
//        BlogPostHashTags expResult = bphtDao.create(bphtTest);
//
//        Assert.assertNotNull(expResult.getBlogId());
//        
//    }
//
//}