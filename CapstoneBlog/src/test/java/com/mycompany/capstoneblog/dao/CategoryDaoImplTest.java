//package com.mycompany.capstoneblog.dao;
//
//import com.mycompany.capstoneblog.dto.Category;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//import java.util.List;
//
//import static org.junit.Assert.*;
//import org.springframework.jdbc.core.JdbcTemplate;
//
//
//public class CategoryDaoImplTest {
//    CategoryDao categoryDao;
//
//    Category testA = new Category();
//
//    public CategoryDaoImplTest() {
//
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("testspring-persistence.xml");
//        categoryDao = ctx.getBean("CategoryDao", CategoryDao.class);
//        
//         JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("DELETE FROM categories");
//
//    }
//
//
//
//    @Before
//    public void setUp() throws Exception {
//
//        testA.setCategoryName("Admin");
//        testA.setId(3);
//
//    }
//
//    @After
//    public void tearDown() throws Exception {
//
//    }
//
//    @Test
//    public void create() {
//
//        Category c = categoryDao.create(testA);
//        Assert.assertEquals("Admin", c.getCategoryName());
//
//    }
//
//    @Test
//    public void read() throws Exception {
//
//        Category test = categoryDao.create(testA);
//        
//        Category newTest = categoryDao.read(test.getId());
//        Assert.assertEquals(newTest.getCategoryName(), "Admin");
//
//    }
//
//    @Test
//    public void update() throws Exception {
//        testA.setCategoryName("hello");
//        System.out.println("Running test for update");
//        categoryDao.update(testA);
//        System.out.println(testA.getCategoryName());
//        Assert.assertEquals("hello", testA.getCategoryName());
//
//    }
//
//    @Test
//    public void delete() throws Exception {
//      //WORKS, COMMENTED OUT TO COMPILE NOW THAT 4 IS DELETED
//
////        Category d = categoryDao.read(4);
////        categoryDao.delete(d);
//
//    }
//
//    @Test
//    public void listAll() throws Exception {
//        
//        categoryDao.create(testA);
//        
//        List<Category> c =  categoryDao.listAll();
//        
//        Assert.assertEquals(c.size(),1);
//
//    }
//
//}