///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycompany.capstoneblog.dao;
//
//import com.mycompany.capstoneblog.dto.StaticPage;
//import java.util.Date;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.jdbc.core.JdbcTemplate;
//
///**
// *
// * @author apprentice
// */
//public class StaticPageDaoImplTest {
//    StaticPageDao staticPageDao;
//    StaticPage staticPageTest1 = new StaticPage();
//    //StaticPage test2 = new StaticPage();
//
//    public StaticPageDaoImplTest() {
//
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("testspring-persistence.xml");
//        staticPageDao = ctx.getBean("StaticPageDao", StaticPageDao.class);
//
//        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("DELETE FROM static_page");
//
//    }
//
//    @BeforeClass
//    public static void setUpClass() {
//
//
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() {
//        staticPageTest1.setId(5);
//        staticPageTest1.setBody("body");
//        staticPageTest1.setActive("Active");
//
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of create method, of class StaticPageDaoImpl.
//     */
//    @Test
//    public void testCreate() {
//
//        StaticPage expResult = staticPageDao.create(staticPageTest1);
//        StaticPage result = staticPageDao.create(staticPageTest1);
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of read method, of class StaticPageDaoImpl.
//     */
//    @Test
//    public void testRead() {
//        StaticPage staticPage = staticPageDao.create(staticPageTest1);
//        StaticPage staticPageRead = staticPageDao.read(staticPage.getId());
//
//        assertEquals(staticPageRead.getBody(), "body");
//
//    }
//
//    /**
//     * Test of update method, of class StaticPageDaoImpl.
//     */
//    @Test
//    public void testUpdate() {
//
//        StaticPage staticPage = staticPageDao.create(staticPageTest1);
//
//        staticPageDao.update(staticPage);
//
//    }
//
//    /**
//     * Test of delete method, of class StaticPageDaoImpl.
//     */
//    @Test
//    public void testDelete() {
//        System.out.println("delete");
//        StaticPage staticPage = staticPageDao.create(staticPageTest1);
//
//        staticPageDao.delete(staticPage);
//    }
//
//    /**
//     * Test of listAll method, of class StaticPageDaoImpl.
//     */
//    @Test
//    public void testListAll() {
//
//        staticPageDao.create(staticPageTest1);
//
//        List<StaticPage> result = staticPageDao.listAll();
//        assertEquals(result.size(), 1);
//
//    }
//
//    @Test
//    public void testActivePages() {
//
//        staticPageDao.create(staticPageTest1);
//
//        List<StaticPage> result = staticPageDao.listActivePages();
//        assertEquals(result.size(), 1);
//
//    }
//
//
//
//}
