///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycompany.capstoneblog.dao;
//
//import com.mycompany.capstoneblog.dto.Authority;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.jdbc.core.JdbcTemplate;
//
///**
// *
// * @author apprentice
// */
//public class AuthorityDaoImplTest {
//    AuthorityDao authorityDao;
//    Authority test = new Authority();
//    
//    public AuthorityDaoImplTest() {
//        
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("testspring-persistence.xml");
//        authorityDao = ctx.getBean("AuthorityDao", AuthorityDao.class);
//
//        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("DELETE FROM authorities");
//        
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//        test.setId(1);
//        test.setUserName("joe");
//        test.setAuthority("ROLE_ADMIN");
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of create method, of class AuthorityDaoImpl.
//     */
//    @Test
//    public void testCreate() {
//        System.out.println("create");
//        Authority authority = authorityDao.create(test);
//     
//        Authority result = authorityDao.create(authority);
//        assertEquals("joe", authority.getUserName());
//     
//    }
//
//    /**
//     * Test of read method, of class AuthorityDaoImpl.
//     */
//    @Test
//    public void testRead() {
//        System.out.println("read");
//        Authority authority = authorityDao.create(test);
//        
//        authorityDao.read(test.getId());
//        
//        assertEquals("ROLE_ADMIN", test.getAuthority());
//        
//    }
//
//    /**
//     * Test of update method, of class AuthorityDaoImpl.
//     */
////    @Test
////    public void testUpdate() {
////        System.out.println("update");
////        
////        test.setId(1);
////        test.setUserName("billy");
////        test.setAuthority("ROLE_ADMIN");
////        
////        authorityDao.update(test);
////        
////        assertEquals("billy", test.getUserName());
////       
////    }
////
////    /**
////     * Test of delete method, of class AuthorityDaoImpl.
////     */
////    @Test
////    public void testDelete() {
////        System.out.println("delete");
////        Authority authority = authorityDao.create(test);
////        authorityDao.delete(authority);
////        
////        
////       
////    }
//
//    /**
//     * Test of listAll method, of class AuthorityDaoImpl.
//     */
//    @Test
//    public void testListAll() {
//        System.out.println("listAll");
//        authorityDao.create(test);
//        List<Authority> a = authorityDao.listAll();
//        
//        assertEquals(a.size(), 1);
//      
//    }
//    
//}
