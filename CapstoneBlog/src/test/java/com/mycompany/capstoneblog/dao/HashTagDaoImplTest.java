///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycompany.capstoneblog.dao;
//
//import com.mycompany.capstoneblog.dto.HashTag;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.jdbc.core.JdbcTemplate;
//
///**
// *
// * @author apprentice
// */
//public class HashTagDaoImplTest {
//    HashTagDao hashTagDao;
//    HashTag test = new HashTag();
//    
//    public HashTagDaoImplTest() {
//        
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("testspring-persistence.xml");
//        hashTagDao = ctx.getBean("HashTagDao", HashTagDao.class);
//        
//        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("DELETE FROM hash_tag");
//        
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//        
//        
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//        test.setId(1);
//        test.setTag("#mergeconflicts");
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of create method, of class HashTagDaoImpl.
//     */
//    @Test
//    public void testCreate() {
//        
//        HashTag result = hashTagDao.create(test);
//        HashTag expResult = hashTagDao.create(test);
//        assertEquals(expResult, result);
//       
//    }
//
//    /**
//     * Test of read method, of class HashTagDaoImpl.
//     */
//    @Test
//    public void testRead() {
//    
//        HashTag instance = hashTagDao.create(test);
//        HashTag result = hashTagDao.read(instance.getId());
//        assertEquals(result.getTag(), "#mergeconflicts");
//   
//    }
//
//    /**
//     * Test of update method, of class HashTagDaoImpl.
//     */
//    @Test
//    public void testUpdate() {
//       
//        HashTag hashTag = hashTagDao.create(test);
//        hashTag.setTag("#capstone");
//        hashTagDao.update(hashTag);
//        assertEquals(hashTag.getTag(), "#capstone");
//       
//    }
//
//    /**
//     * Test of delete method, of class HashTagDaoImpl.
//     */
//    @Test
//    public void testDelete() {
//        System.out.println("delete");
//        HashTag hashTag = hashTagDao.create(test);
//        hashTagDao.delete(hashTag);
//        
//      
//    }
//
//    /**
//     * Test of listAll method, of class HashTagDaoImpl.
//     */
//    @Test
//    public void testListAll() {
//        System.out.println("listAll");
//        HashTag instance = hashTagDao.create(test);
//        
//        List<HashTag> result = hashTagDao.listAll();
//        assertEquals(result.size(), 1);
//       
//    }
//    
//}
