<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <jsp:include page="navbar.jsp"/>
    <script>

        <%--body {--%>
        <%--background-image: url("${pageContext.request.contextPath}/images/bowlingsketch.jpeg");--%>
        <%--/*background-repeat: no-repeat;*/--%>
        <%--/*background-position: right top;*/--%>
        <%--/*background-attachment: fixed;*/--%>
        <%--}--%>

    </script>

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>

    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <script>
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
        });
    </script>


</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <table class="table table-responsive" id="blogTable">
                <tr>
                    <th></th>

                </tr>
                <c:forEach items="${blogPostList}" var="blogPost">
                    <tr id="blogpost-row-${blogPost.id}">
                        <td>Published: ${blogPost.publishDate}${blogPost.body}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>


        <div class="col-sm-4">
            <table class="table table-responsive">
                <tr>
                    <th>Categories</th>
                </tr>
                <c:forEach items="${categoryList}" var="category">
                    <tr>
                        <td>
                            <a href="${pageContext.request.contextPath}/categories/${category.id}">${category.categoryName}</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>

        </div>
    </div>
</div>

</body>

</div>


<script>
    var contextRoot = "${pageContext.request.contextPath}";
</script>

<script src="${pageContext.request.contextPath}/js/deleteBlogPost.js"></script>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>


</html>










