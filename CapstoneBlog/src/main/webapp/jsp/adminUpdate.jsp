<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <jsp:include page="navbar.jsp"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/chosen.min.css">

</head>

<body>

<div class="container">

    <form class="form-horizontal" action="${pageContext.request.contextPath}/blogpost/update"
          method="POST" id="form">

        <input type="hidden" name="id" id="updateId" value="${blog.id}"/>

        <div class="form-group">
            <label for="updateTitle" class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
                <h2>Update Blog Post</h2>
            </div>
        </div>

        <div class="form-group">
            <label for="updateTitle" class="col-sm-2 control-label">Blog Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="updateTitle" placeholder="Title" value="${blog.title}">
            </div>
            <div class="form-group">
                <label for="updateApproval">Place in admin blog list?</label>
                <select class="form-control" id="updateApproval">
                    <option value="2">No</option>
                    <option value="0">Yes</option>
                </select>
            </div>
            <div class="form-group">
                <label for="mytextarea" class="col-sm-2 control-label">Text Body</label>
                <div class="col-sm-10">
                            <textarea id="mytextarea" class="tinymce" rows="1" cols="10" name="qa" form="form"
                                      value="${blog.body}">${blog.body}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="updatePublishDate" class="col-sm-2 control-label">Publish Date</label>
                <div class="col-sm-10">
                    <input type="text" class="datepicker" id="updatePublishDate" placeholder="Publish Date"
                           value="${blog.publishDate}">
                </div>
            </div>
            <div class="form-group">
                <label for="updateExpirationDate" class="col-sm-2 control-label">Expiration Date</label>
                <div class="col-sm-10">
                    <input type="text" class="datepicker" id="updateExpirationDate" placeholder="Expiration Date"
                           value="${blog.expirationDate}">
                </div>
            </div>
            <div class="form-group">
                <label for="updateCategories">Categories</label>
                <select id="updateCategories" style="width:350px;" multiple class="chosen-select form-control"
                        name="faculty">
                    <c:forEach items="${categoryList}" var="category">
                        <option selected value="${category.categoryName}">${category.categoryName}</option>
                    </c:forEach>
                    <c:forEach items="${otherCategories}" var="cat">
                        <option value="${cat.categoryName}">${cat.categoryName}</option>
                    </c:forEach>
                </select>
            </div>

            <%--Displays vailidation errors if they occur--%>

            <div id="update-blogpost-validation-errors" style="color:red;"></div>

            <%--This div and script are used to display a home button and updates confirmation if changes are made--%>

            <div class="row">
                <div class="col-sm-4" id="confirmationUpdate"></div>
                <div class="col-sm-4" id="targetElement"></div>
            </div>

            <script id="homeRedirect" language="text">
                         <input type = "button" class = "btn btn-primary" id = "backToHome" value = "Home" onclick = "location.href =${pageContext.request.contextPath}  />
                    </script>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type="button" id="updateBlogPostSubmitButton" value="Update Blog" class="btn btn-default"/>
                        </div>
                    </div>
            </form>

        </div>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js">
            </script>

            <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

            <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

            <script>
                $(function () {
                    $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
                });
            </script>
            <script>
                var contextRoot = "${pageContext.request.contextPath}";
            </script>

            <script>
                $(function () {
                    $(".chosen-select").chosen();
                });
            </script>

            <script src="${pageContext.request.contextPath}/js/chosen.jquery.min.js"></script>

            <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
            <script src="${pageContext.request.contextPath}/js/addBlogPost.js"></script>
            <script src="${pageContext.request.contextPath}/js/updateBlogPost.js"></script>
            <script src="${pageContext.request.contextPath}/js/init-tinymce.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                    crossorigin="anonymous"></script>

</body>
</html>