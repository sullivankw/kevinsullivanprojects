<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/chosen.min.css">

    <jsp:include page="navbar.jsp"/>

</head>
<body>

<%--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!MARKETER TABLE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! --%>
<div class="container">
    <h3>Marketer Posts Awaiting Approval</h3>

    <table class="table table-responsive" id="blogMarketerTable">

        <tr>
            <th>Title</th>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <th>Update</th>
            </sec:authorize>
            <th>Delete</th>
        </tr>

        <c:forEach items="${marketList}" var="marketPost">
            <tr id="blogpost-market-row-${marketPost.id}">
                <td><a href="${pageContext.request.contextPath}/blogpost/show/${marketPost.id}">${marketPost.title}</a>
                </td>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <td><a href="${pageContext.request.contextPath}/blogpost/update/${marketPost.id}">Update</a></td>
                </sec:authorize>
                <td><a data-blogpost-id="${marketPost.id}" data-toggle="modal"
                       data-target="#deleteBlogPostModal">Delete</a></td>

            </tr>
        </c:forEach>

    </table>

</div>

<%--!!!!!!!!!!!!!!!!!!!!!!!!!!!!table for marketer blog post adds!!!!!!!!!!!!!!!!!!!!!!!!!!--%>
<div class="container">
    <div class="row">

        <div class="col-xs-8">

            <h3>Marketer Add Blog Post Form</h3>
            <form id="formMark" name="blog" action="${pageContext.request.contextPath}/blogpost" method="post">
                <div class="form-group">
                    <label for="blogTitleMark">Enter Title</label>
                    <input class="form-control" id="blogTitleMark" type="text"/>
                </div>
                <input type="hidden" id="approvalMark" value="2"/>
                <div class="form-group">
                    <label for="publishdateMark">Enter Publish Date</label>
                    <input id="publishdateMark" class="datepicker form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="expirationdateMark">Enter Expiration Date </label>
                    <input id="expirationdateMark" class="datepicker form-control" type="text"/></p>
                </div>
                <div class="form-group">
                    <select id="categorySelectMark" style="width:350px;" multiple class="chosen-select form-control"
                            name="faculty">
                        <c:forEach items="${categories}" var="category">
                            <option value="${category.categoryName}">${category.categoryName}</option>
                        </c:forEach>
                    </select>
                </div>
                <input type="button" data-toggle="modal" data-target="#addCategory" value="Add Category"/>

                <textarea id="mytextarea" class="tinymce" rows="1" cols="10">Enter text here...</textarea>


                <input type="button" id="SendToAdminButton" value="Send Blog Post For Review"/>

                <%--<div id="create-blogpost-validation-errors" style="color:red;"></div>--%>

            </form>

        </div>

    </div>


    <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                </div>
                <div class="modal-body">

                    <table>
                        <tr>
                            <th>New Category:</th>
                            <td><input type="text" id="newCategory"/></td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="addCategoryButton">Add
                        Category
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteBlogPostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Blog Post</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="deleteId"/>

                    <table>
                        <tr>
                            <th>Title:</th>
                            <td id="deleteTitle"></td>
                        </tr>
                        <tr>
                            <th>Approval Status:</th>
                            <td id="deleteApprovalStatus"></td>
                        </tr>
                        <tr>
                            <th>Author Id:</th>
                            <td id="deleteAuthorId"></td>
                        </tr>
                        <tr>
                            <th>Publish Date:</th>
                            <td id="deletePublishDate"></td>
                        </tr>
                        <tr>
                            <th>Expiration Date:</th>
                            <td id="deleteExpirationDate"></td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="deleteBlogPostButton">Delete Blog Post</button>
                </div>
            </div>
        </div>
    </div>


    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>

    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <script>
        var contextRoot = "${pageContext.request.contextPath}";
    </script>

    <script src="${pageContext.request.contextPath}/js/chosen.jquery.min.js"></script>

    <script>
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
        });
    </script>

    <script>
        $(function () {
            $(".chosen-select").chosen();
        });
    </script>

    <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>


    <script src="${pageContext.request.contextPath}/js/deleteBlogPost.js"></script>

    <script src="${pageContext.request.contextPath}/js/addCategory.js"></script>

    <script src="${pageContext.request.contextPath}/js/sendBlogPostToAdmin.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/init-tinymce.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

</body>
</html>