<%-- 
    Document   : adminHome
    Created on : Oct 6, 2016, 8:24:59 PM
    Author     : apprentice
--%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/chosen.min.css">

    <jsp:include page="navbar.jsp"/>

</head>
<body>

<div class="container">

    <div class="row">

        <div class="col-xs-8">


            <div class="col-xs-12">

                <table class="table table-responsive" id="blogTable">

                    <tr>
                        <th>Title</th>
                        <th>Publish Date</th>
                        <th>Expiration Date</th>
                        <th>Update</th>
                        <th>Delete</th>
                    </tr>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <c:forEach items="${blogPostList}" var="blogPost">
                            <tr id="blogpost-row-${blogPost.id}">
                                <td>
                                    <a href="${pageContext.request.contextPath}/blogpost/show/${blogPost.id}">${blogPost.title}</a>
                                </td>
                                <td>${blogPost.publishDate}</td>
                                <td>${blogPost.expirationDate}</td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/blogpost/update/${blogPost.id}">Update</a>
                                </td>
                                <td><a data-blogpost-id="${blogPost.id}" data-toggle="modal"
                                       data-target="#deleteBlogPostModal">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </sec:authorize>
                </table>
            </div>
        </div>
        <sec:authorize access="hasRole('ROLE_ADMIN')">

        <div class="row">

            <div class="col-xs-8">

                <h3>Add Blog Post</h3>

                <form id="form" name="blog" action="${pageContext.request.contextPath}/blogpost" method="post">
                    <div class="form-group">
                        <label for="blogTitle">Enter Title</label>
                        <input class="form-control" id="blogTitle" type="text"/>
                    </div>
                        <%--!!!!!!!!!!!!!!!!!!!!!!!!!!!!this number zero sets this as a admin blog post!!!!!!!!!!!!!!!!!!!!!!!!!!--%>
                    <input type="hidden" id="approval" value="0"/>
                    <div class="form-group">
                        <label for="publishdate">Enter Publish Date</label>
                        <input id="publishdate" class="datepicker form-control" type="text"/>
                    </div>
                    <div class="form-group">
                        <label for="expirationdate">Enter Expiration Date </label>
                        <input id="expirationdate" class="datepicker form-control" type="text"/></p>
                    </div>
                    <div class="form-group">
                        <select id="categorySelect" style="width:350px;" multiple class="chosen-select form-control"
                                name="faculty">
                            <c:forEach items="${categories}" var="category">
                                <option value="${category.categoryName}">${category.categoryName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <input type="button" data-toggle="modal" data-target="#addCategory" value="Add Category"/>

                    <textarea id="mytextarea" class="tinymce" rows="1" cols="10">Enter text here...</textarea>

                    <input type="button" id="CreateBlogPostButton" value="Add Blog Post"/>

                    <div id="create-blogpost-validation-errors" style="color:red;"></div>

                    </table>
            </div>
        </div>

        <div class="col-xs-4">
            <div>
                <form id="form" name="user" action="${pageContext.request.contextPath}/user" method="post">

                    <table class="table table-responsive">
                        <tr>
                            <th>Create New User</th>
                        </tr>
                        <tr>
                            <td>Enter User Name <input type="text" id="userName"></td>
                        </tr>
                        <tr>
                            <th>Create User Password</th>
                        </tr>
                        <tr>
                            <td>Enter User Password <input type="text" id="password"></td>
                        </tr>

                        <tr>
                            <th>Select User Authority</th>
                        </tr>
                        <tr>
                            <td>Select User Authority
                                <select id="role">
                                    <option value="ROLE_ADMIN">ROLE_ADMIN</option>
                                    <option value="ROLE_USER">ROLE_USER</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th>Enable User</th>
                        </tr>
                        <tr>
                            <td>Enable User
                                <select id="enabled">
                                    <option value="1">Activate User</option>
                                    <option value="0">Deactivate User</option>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="button" id="CreateUserButton" value="Create"/></td>
                        </tr>
                    </table>
                    <br>
                </form>
            </div>

            <div class="row">

                <div>

                    <table class="table table-responsive" id="userTable">
                        <tr>
                            <th>User Name</th>
                            <th>Enabled</th>
                            <th>Authority</th>
                            <!--                                <th>Password</th>-->
                            <th>Update User</th>
                            <th>Remove User</th>
                        </tr>
                        <c:forEach items="${userList}" var="user">
                            <tr>
                                <td>${user.userName}</td>
                                <td>${user.enabled}</td>
                                <td>${user.role}</td>
                                <td><a data-user-id="${user.id}" data-toggle="modal" data-target="#updateUserModal">Update</a>
                                </td>
                                <td><a data-user-id="${user.id}" data-toggle="modal" data-target="#deleteUserModal">Delete</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                </div>
                <div class="modal-body">

                    <table>
                        <tr>
                            <th>New Category:</th>
                            <td><input type="text" id="newCategory"/></td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="addCategoryButton">Add
                        Category
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteBlogPostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Blog Post</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="deleteId"/>

                    <table>
                        <tr>
                            <th>Title:</th>
                            <td id="deleteTitle"></td>
                        </tr>
                        <tr>
                            <th>Approval Status:</th>
                            <td id="deleteApprovalStatus"></td>
                        </tr>
                        <tr>
                            <th>Author Id:</th>
                            <td id="deleteAuthorId"></td>
                        </tr>
                        <tr>
                            <th>Publish Date:</th>
                            <td id="deletePublishDate"></td>
                        </tr>
                        <tr>
                            <th>Expiration Date:</th>
                            <td id="deleteExpirationDate"></td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="deleteBlogPostButton">Delete Blog Post</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="deleteId"/>

                    <table>
                        <tr>
                            <th>User Name:</th>
                            <td id="deleteUserName"></td>
                        </tr>
                        <tr>
                            <th>Password:</th>
                            <td id="deletePassword"></td>
                        </tr>
                        <tr>
                            <th>Authority:</th>
                            <td id="deleteAuthority"></td>
                        </tr>
                        <tr>
                            <th>Status:</th>
                            <td id="deleteEnabled"></td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="deleteUserButton">Delete User</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updateUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Update User</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="updateId"/>

                    <table>
                        <tr>
                            <th>User Name:</th>
                            <td><input type="text" id="updateUserName"/></td>
                        </tr>
                        <tr>
                            <th>Password:</th>
                            <td><input type="text" id="updatePassword"/></td>
                        </tr>
                        <tr>
                            <th>Authority:</th>
                            <td><select id="updateAuthority">
                                <option value="ROLE_ADMIN">ROLE_ADMIN</option>
                                <option value="ROLE_USER">ROLE_USER</option>
                            </select></td>
                        </tr>
                        <tr>
                            <th>Status:</th>
                            <td><select id="updateStatus">
                                <option value="1">Activate User</option>
                                <option value="0">Deactivate User</option>
                            </select></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="updateUserButton">Update User</button>
                </div>
            </div>
        </div>
    </div>

        <%--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!MARKETER TABLE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! --%>
    <div class="container">
        <div class="row">
            <h3>Marketer Posts For Approval</h3>

            <table class="table table-responsive" id="blogMarketerTable">

                <tr>
                    <th>Title</th>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <th>Update</th>
                    </sec:authorize>
                    <th>Delete</th>
                </tr>

                <c:forEach items="${marketList}" var="marketPost">
                    <tr id="blogpost-market-row-${marketPost.id}">
                        <td>
                            <a href="${pageContext.request.contextPath}/blogpost/show/${marketPost.id}">${marketPost.title}</a>
                        </td>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <td><a href="${pageContext.request.contextPath}/blogpost/update/${marketPost.id}">Update</a>
                            </td>
                        </sec:authorize>
                        <td><a data-blogpost-id="${marketPost.id}" data-toggle="modal"
                               data-target="#deleteBlogPostModal">Delete</a></td>

                    </tr>
                </c:forEach>

            </table>
        </div>
    </div>
    </sec:authorize>


    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>

    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <script>
        var contextRoot = "${pageContext.request.contextPath}";
    </script>

    <script src="${pageContext.request.contextPath}/js/chosen.jquery.min.js"></script>

    <script>
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
        });
    </script>

    <script>
        $(function () {
            $(".chosen-select").chosen();
        });
    </script>

    <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>

    <script src="${pageContext.request.contextPath}/js/addBlogPost.js"></script>

    <script src="${pageContext.request.contextPath}/js/userCrud.js"></script>

    <script src="${pageContext.request.contextPath}/js/deleteBlogPost.js"></script>

    <script src="${pageContext.request.contextPath}/js/addCategory.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/init-tinymce.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

</body>
</html>
