<%-- 
    Document   : login
    Created on : Oct 14, 2016, 2:56:30 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>Login</h1>
        
        <hr/>
        
    <c:if test="${param.login_error == 1}">
    
        <h2>Error Logging In. Please Retry</h2>
        
    </c:if>
        
        <form action="${pageContext.request.contextPath}/j_spring_security_check" method="POST">
        
            Username:<input type="text" name="username" />
            Password:<input type="password" name="password"/>
            
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            
            <input type="submit" value="Login"/>
            
        </form>
        
    </body>
</html>
