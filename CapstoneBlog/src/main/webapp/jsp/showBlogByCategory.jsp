<%-- 
    Document   : showBlogByCategory
    Created on : Oct 13, 2016, 3:08:23 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <jsp:include page="navbar.jsp"/>

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>

</head>
<body>

<div class="container">
    <div class="col-xs-6">
        <table class="table table-responsive" id="blogTable">
            <tr>
                <th></th>
            </tr>
            <c:forEach items="${bpList}" var="blogPost">
                <tr id="blogpost-row-${blogPost.id}">
                    <td>${blogPost.body}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>
