$(document).ready(function(){
    $('#CreateUserButton').on('click', function(e) {
        
        e.preventDefault();

        var myUser = {
            userName: $('#userName').val(),
            password: $('#password').val(),
            enabled: $('#enabled').val(),
            role: $('#role').val()
            
        };
//        var myAuthority = {
//          userName: $('#userName').val(),
//          authority: $('#role').val()
//        };
 
 
        var myUserData = JSON.stringify(myUser);
        console.log(contextRoot);
        
        
        $.ajax({
            url: contextRoot + "/user",
            type: "POST",
            data: myUserData,
            dataType: "json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function(data, status) {
                
                var tableRow = buildRow(data);
                
                
                console.log(tableRow);
                
                $('#userTable').append($(tableRow));
                $('#role').val('');
                $('#userName').val('');
                $('#password').val('');
                $('#enabled').val('');

            },
            error: function(data, status) {
                var errors = data.responseJSON.errors;
                
                $.each(errors, function (index, error) {
                    $('#add-user-validation-errors').append(" " + error.message + "<br>");
                
                });
                
            }
        });
        
    });

    $('#deleteUserModal').on('shown.bs.modal', function (e) {
        var link = $(e.relatedTarget);

        var userId = link.data('user-id');
        console.log(userId);

        $.ajax({
            url: contextRoot + "/user/" + userId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json")
            },
            success: function (data, status) {
                $('#deleteId').val(data.id);
                $('#deleteUserName').text(data.userName);
                $('#deletePassword').text(data.password);
                $('#deleteAuthority').text(data.role);
                $('#deleteEnabled').text(data.enabled);
               
            },
            error: function (data, status) {
                alert("USER NOT FOUND");
            }
        });
    });
    
     $('#deleteUserButton').on('click', function (e) {

        e.preventDefault();

        var myUser = {
            id: $('#deleteId').val(),
            userName: $('#deleteUserName').text(),
            password: $('#deletePassword').text(),
            role: $('#deleteAuthority').text(),
            enabled: $('#deleteEnabled').text()
        };
        console.log(myUser.id);

        var myUserData = JSON.stringify(myUser);
        console.log(contextRoot);


        $.ajax({
            url: contextRoot + "/user/" + myUser.id,
            type: "DELETE",
            data: myUserData,
//            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                // var tableRow = buildContactRow(data);
//                console.log(myUser.id);
                
                $('#user-row-' + myUser.id).remove();

                $('#deleteUserModal').modal('hide');

            },
            error: function (data, status) {
                console.log("ERROR");
            }
        });
    });
    
    $('#updateUserModal').on('shown.bs.modal', function (e) {
        var link = $(e.relatedTarget);

        var userId = link.data('user-id');
        console.log(userId);

        $.ajax({
            url: contextRoot + "/user/" + userId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json")
            },
            success: function (data, status) {
                $('#updateId').val(data.id);
                $('#updateUserName').val(data.userName);
                $('#updatePassword').val(data.password);
                $('#updateAuthority').val(data.role);
                $('#updateStatus').val(data.enabled);
               
            },
            error: function (data, status) {
                alert("USER NOT FOUND");
            }
        });
    });
    
    $('#updateUserButton').on('click', function (e) {

        e.preventDefault();

        var myUser = {
            id: $('#updateId').val(),
            userName: $('#updateUserName').val(),
            password: $('#updatePassword').val(),
            role: $('#updateAuthority').val(),
            enabled: $('#updateStatus').val()
        };
        console.log(myUser.id);

        var myUserData = JSON.stringify(myUser);
        console.log(contextRoot);

        $.ajax({
            url: contextRoot + "/user/" + myUser.id,
            type: "PUT",
            data: myUserData,
//            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                 var tableRow = buildRow(data);
//                console.log(myUser.id);
                
                $('#user-row-' + myUser.id).replaceWith( $(tableRow)  );

                $('#updateUserModal').modal('hide');

            },
            error: function (data, status) {
                console.log("ERROR");
            }
        });
    });
    
    function buildRow(data) {
        var tableRow = '\
                    <tr id="user-row-'+ data.id +'"> \n\
                    <td>' + data.userName + '</td> \n\
                    <td>' + data.enabled + '</td> \n\
                    <td>' + data.role + '</td> \n\
                    <td><a data-user-id="' + data.id + '" data-toggle="modal" data-target="#updateUserModal">Update</a></td> \n\
                    <td><a data-user-id="' + data.id + '" data-toggle="modal" data-target="#deleteUserModal">Delete</a></td> \n\
                    </tr>';
        
        return tableRow;
    }
    
});