
window.onload = function () {

    var btn = document.getElementById('SendToAdminButton');

    btn.onclick = function () {

        tinymce.triggerSave();

        //$('#create-blogpost-validation-errors').text('');

        var myBlogPost = {
            title: $('#blogTitleMark').val(),
            body: $('#mytextarea').val(),
            approvalStatus: $('#approvalMark').val(),
            publishDate: $('#publishdateMark').val(),
            expirationDate: $('#expirationdateMark').val(),
            category: $('#categorySelectMark').val()

        };

        console.log(myBlogPost.category);
        console.log(myBlogPost.approvalStatus);

        var myBlogPostData = JSON.stringify(myBlogPost);
        console.log(myBlogPostData);

        $.ajax({
            url: contextRoot + "/blogpost",
            type: "POST",
            data: myBlogPostData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                var tableRow = buildBlogPostRow(data);

                $('#blogMarketerTable tbody').append(tableRow);

                $('#mytextarea').val('');
                console.log(myBlogPostData);

            },
            error: function (data, status) {

                // var errors = data.responseJSON.errors;
                //
                // $.each(errors, function (index, error) {
                //     $('#create-blogpost-validation-errors').append('<li>' + error.message + '</li>');
                //
                // });

                console.log("An error occurred while submitting blog post.");

            }

        });
    };



    function buildBlogPostRow(data) {


        var tableRow = '\
                        <tr id="blogpost-market-row-' + data.id + '"> \n\
                            <td><a href="/CapstoneBlog/blogpost/show/' + data.id + '">' + data.title + '</a></td> \n\
                            <td><a href="/CapstoneBlog/blogpost/update/' + data.id + '">Update</a></td> \n\
                            <td><a data-blogpost-id="' + data.id + '" data-toggle="modal" data-target="#deleteBlogPostModal">Delete</a></td> \n\
                        </tr>';

        return tableRow;
    }

};