
window.onload = function () {

    var btn = document.getElementById('CreateBlogPostButton');

    btn.onclick = function () {

        tinymce.triggerSave();

        $('#create-blogpost-validation-errors').text('');

        var myBlogPost = {
            title: $('#blogTitle').val(),
            body: $('#mytextarea').val(),
            approvalStatus: $('#approval').val(),
            publishDate: $('#publishdate').val(),
            expirationDate: $('#expirationdate').val(),
            category: $('#categorySelect').val()

        };

        console.log(myBlogPost.category);
        console.log(myBlogPost.approvalStatus);


        var myBlogPostData = JSON.stringify(myBlogPost);

        $.ajax({
            url: contextRoot + "/blogpost",
            type: "POST",
            data: myBlogPostData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                $('#htmlDiv').append(data.body);

                var tableRow = buildBlogPostRow(data);

                $('#blogTable tbody').append(tableRow);

                $('#mytextarea').val('');



            },
            error: function (data, status) {

                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {
                    $('#create-blogpost-validation-errors').append('<li>' + error.message + '</li>');

                });

                console.log("An error occurred while submitting blog post.");

            }

        });
    };



    function buildBlogPostRow(data) {

        var newPublishDate = new Date(data.publishDate);

        newPublishDate = newPublishDate.getFullYear() + '-' + (newPublishDate.getMonth() + 1) + '-' + (newPublishDate.getDate() + 1);

        var newExpirationDate = new Date(data.expirationDate);

        newExpirationDate = newExpirationDate.getFullYear() + '-' + (newExpirationDate.getMonth() + 1) + '-' + (newExpirationDate.getDate() + 1);

        var tableRow = '\
                        <tr id="blogpost-row-' + data.id + '"> \n\
                            <td><a href="/CapstoneBlog/blogpost/show/' + data.id + '">' + data.title + '</a></td> \n\
                            <td>' + newPublishDate + '</td> \n\
                            <td>' + newExpirationDate + '</td> \n\
                            <td><a href="/CapstoneBlog/blogpost/update/' + data.id + '">Update</a></td> \n\
                            <td><a data-blogpost-id="' + data.id + '" data-toggle="modal" data-target="#deleteBlogPostModal">Delete</a></td> \n\
                        </tr>';

        return tableRow;
    }

};

