/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dto;

/**
 *
 * @author apprentice
 */
public class BlogPostHashTags {
    
    private Integer id;
    private Integer blogId;
    private Integer hashTagId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    

    public Integer getBlogId() {
        return blogId;
    }

    public void setBlogId(Integer blogId) {
        this.blogId = blogId;
    }

    public Integer getHashTagId() {
        return hashTagId;
    }

    public void setHashTagId(Integer hashTagId) {
        this.hashTagId = hashTagId;
    }
    
    
    
}
