/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.controllers;

import com.mycompany.capstoneblog.dao.BlogPostDao;
import com.mycompany.capstoneblog.dao.CategoryDao;
import com.mycompany.capstoneblog.dao.HashTagDao;
import com.mycompany.capstoneblog.dao.StaticPageDao;
import com.mycompany.capstoneblog.dao.UserDao;
import com.mycompany.capstoneblog.dto.BlogPost;
import com.mycompany.capstoneblog.dto.Category;
import com.mycompany.capstoneblog.dto.HashTag;
import com.mycompany.capstoneblog.dto.StaticPage;
import com.mycompany.capstoneblog.dto.User;

import java.util.List;
import java.util.Map;
import javax.inject.Inject;

import com.mycompany.capstoneblog.dto.StaticPage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class AdminHomeController {

    private BlogPostDao blogPostDao;
    private HashTagDao hashTagDao;
    private UserDao userDao;
    private CategoryDao categoryDao;
    private StaticPageDao staticDao;

    @Inject
    public AdminHomeController(StaticPageDao staticDao, CategoryDao categoryDao, BlogPostDao blogPostDao, HashTagDao hashTagDao, UserDao userDao) {
        this.blogPostDao = blogPostDao;
        this.hashTagDao = hashTagDao;
        this.userDao = userDao;
        this.categoryDao = categoryDao;
        this.staticDao = staticDao;

    }

    @RequestMapping(value = "/adminhome", method = RequestMethod.GET)
    public String home(Map model) {


        List<BlogPost> blogPosts = blogPostDao.listAll();
        List<HashTag> hashies = hashTagDao.listAll();
        List<StaticPage> stashies = staticDao.listActivePages();
        List<User> user = userDao.listAll();
        List<Category> categories = categoryDao.listAll();

        List<BlogPost> blogPostsMarketerList = blogPostDao.listPendingPosts();
        
        model.put("blog", new BlogPost());
        model.put("blogPostList", blogPosts);
        model.put("user", new User());
        model.put("userList", user);
        model.put("categories", categories);
        model.put("stashiesList", stashies);
        model.put("marketList", blogPostsMarketerList);

        return "adminHome";
    }

}
