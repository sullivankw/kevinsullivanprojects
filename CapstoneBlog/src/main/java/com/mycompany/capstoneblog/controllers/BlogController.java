/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.controllers;

import com.mycompany.capstoneblog.dao.BlogPostCategoryDao;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import com.mycompany.capstoneblog.dao.BlogPostDao;
import com.mycompany.capstoneblog.dao.BlogPostHashTagsDao;
import com.mycompany.capstoneblog.dao.CategoryDao;
import com.mycompany.capstoneblog.dao.HashTagDao;
import com.mycompany.capstoneblog.dto.BlogPost;
import com.mycompany.capstoneblog.dto.BlogPostCategory;
import com.mycompany.capstoneblog.dto.BlogPostHashTags;
import com.mycompany.capstoneblog.dto.Category;
import com.mycompany.capstoneblog.dto.HashTag;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/blogpost")
public class BlogController {
//Test

    private BlogPostDao blogDao;
    private HashTagDao hashDao;
    private BlogPostHashTagsDao blogHashDao;
    private CategoryDao categoryDao;
    private BlogPostCategoryDao blogCatDao;

    @Inject
    public BlogController(BlogPostCategoryDao blogCatDao, BlogPostDao blogDao, HashTagDao hashDao, BlogPostHashTagsDao blogHashDao, CategoryDao categoryDao) {

        this.hashDao = hashDao;
        this.blogDao = blogDao;
        this.blogHashDao = blogHashDao;
        this.categoryDao = categoryDao;
        this.blogCatDao = blogCatDao;

    }

    @RequestMapping(value = "show/{id}", method = RequestMethod.GET)
    public String show(@PathVariable("id") Integer blogPostId, Map Model) {

        BlogPost blogPost = blogDao.read(blogPostId);

        Model.put("blog", blogPost);

        return "adminShow";

    }

    @RequestMapping(value = "/category", method = RequestMethod.POST)
    @ResponseBody
    public Category addCategory(@RequestBody Category category, Map model) {

        Category newCat = categoryDao.create(category);

        List<Category> categories = categoryDao.listAll();

        model.put("categories", categories);

        return newCat;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public BlogPost createBlogPost(@Valid @RequestBody BlogPost blogPost) {

        BlogPost bp = blogDao.create(blogPost);

        addCategories(bp);

        List<String> hashTags = extractHash(bp.getBody());

        boolean isListNull = hashTags.isEmpty();

        if (!isListNull) {
            addHashTags(hashTags, bp);
        }

        return bp;

    }

    void addCategories(BlogPost bp) {
        List<String> allCategories = getCategories();

        List<String> newCategories = bp.getCategory();

        for (String s : newCategories) {

            if (allCategories.contains(s)) {
                BlogPostCategory bpc = new BlogPostCategory();
                Category c = categoryDao.returnByName(s);

                bpc.setBlogId(bp.getId());
                bpc.setCategoryId(c.getId());

                blogCatDao.create(bpc);

                continue;
            }

            Category category = new Category();

            BlogPostCategory b = new BlogPostCategory();

            category.setCategoryName(s);

            Category newCat = categoryDao.create(category);

            b.setBlogId(bp.getId());
            b.setCategoryId(newCat.getId());

            blogCatDao.create(b);

        }
    }

    List<String> getCategories() {
        List<Category> allCategories = categoryDao.listAll();
        List<String> stringCategories = new ArrayList();

        for (Category c : allCategories) {

            stringCategories.add(c.getCategoryName());
        }

        return stringCategories;
    }

    void addHashTags(List<String> hashTags, BlogPost bp) {

        List<String> hashList = getHashTags();

        for (String s : hashTags) {

            if (hashList.contains(s)) {

                BlogPostHashTags b = new BlogPostHashTags();
                HashTag h = hashDao.returnByName(s);

                b.setBlogId(bp.getId());
                b.setHashTagId(h.getId());

                blogHashDao.create(b);

                continue;
            }

            HashTag ht = new HashTag();

            BlogPostHashTags bpht = new BlogPostHashTags();

            ht.setTag(s);

            HashTag newTag = hashDao.create(ht);

            bpht.setBlogId(bp.getId());
            bpht.setHashTagId(newTag.getId());

            blogHashDao.create(bpht);

        }
    }

    List<String> getHashTags() {
        List<HashTag> tags = hashDao.listAll();
        List<String> stringTags = new ArrayList();

        for (HashTag h : tags) {

            stringTags.add(h.getTag());

        }

        return stringTags;
    }

    public List<String> extractHash(String body) {

        Pattern regEx = Pattern.compile("#(\\w+)");

        Matcher matcher = regEx.matcher(body);

        List<String> tags = new ArrayList();

        while (matcher.find()) {
            tags.add(matcher.group(1));
        }

        return tags;

    }

    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") Integer blogPostId, Map model) {

        BlogPost blog = blogDao.read(blogPostId);
        List<BlogPostCategory> list = blogCatDao.returnByBlogId(blog.getId());
        List<Category> masterList = categoryDao.listAll();
        List<Category> selectedCategories = new ArrayList();

        for (BlogPostCategory b : list) {

            Category c = categoryDao.read(b.getCategoryId());

            selectedCategories.add(c);

            ListIterator litr = masterList.listIterator();

            while (litr.hasNext()) {

                Category cat = (Category) litr.next();

                if (c.getId() == cat.getId()) {

                    litr.remove();

                }
            }

        }

        model.put("blog", blog);
        model.put("categoryList", selectedCategories);
        model.put("otherCategories", masterList);

        return "adminUpdate";

    }

    @RequestMapping(value = "update/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public BlogPost editSubmit(@Valid @RequestBody BlogPost blogPost) {

        List<String> newCategories = blogPost.getCategory();

        List<String> allCategories = getCategories();

        List<String> oldCategories = getOldCategories(blogPost);

        deleteUpdatedCategories(oldCategories, newCategories, blogPost);

        addNewUpdatedCategories(newCategories, oldCategories, allCategories, blogPost);

        BlogPost oldPost = blogDao.read(blogPost.getId());

        editHashTags(oldPost, blogPost);

        return blogPost;

    }

    void editHashTags(BlogPost oldPost, BlogPost blogPost) {
        List<String> oldTags = extractHash(oldPost.getBody());

        List<String> allTags = getHashTags();

        List<String> updatedTags = extractHash(blogPost.getBody());

        boolean isListNull = updatedTags.isEmpty();

        if (!isListNull) {
            addNewHashTags(oldTags, updatedTags, allTags, blogPost);
            deleteUpdatedHashTags(oldTags, updatedTags, blogPost);

        } else if (isListNull) {
            blogHashDao.delete(blogPost);
        }

        blogDao.update(blogPost);
    }

    void deleteUpdatedCategories(List<String> oldCategories, List<String> newCategories, BlogPost blogPost) {
        for (String s : oldCategories) {

            Category c = categoryDao.returnByName(s);

            boolean valid = newCategories.contains(s);

            if (!valid) {

                List<BlogPostCategory> bpcList = blogCatDao.returnByCategoryId(c.getId());

                for (BlogPostCategory b : bpcList) {

                    if (b.getBlogId().equals(blogPost.getId())) {
                        blogCatDao.deleteById(b.getId());
                    }
                }
            }

        }
    }

    void deleteUpdatedHashTags(List<String> oldTags, List<String> newTags, BlogPost blogPost) {

        for (String s : oldTags) {

            HashTag ht = hashDao.returnByName(s);

            boolean valid = newTags.contains(s);

            if (!valid) {

                List<BlogPostHashTags> bphtList = blogHashDao.returnByHashTagId(ht.getId());

                for (BlogPostHashTags b : bphtList) {

                    if (b.getBlogId().equals(blogPost.getId())) {
                        blogHashDao.deleteById(b.getId());
                    }
                }
            }

        }
    }

    void addNewUpdatedCategories(List<String> newCategories, List<String> oldCategories, List<String> allCategories, BlogPost blogPost) {
        for (String s : newCategories) {

            if (oldCategories.contains(s)) {

                continue;

            } else if (allCategories.contains(s)) {

                BlogPostCategory bpc = new BlogPostCategory();
                Category category = categoryDao.returnByName(s);

                bpc.setBlogId(blogPost.getId());
                bpc.setCategoryId(category.getId());

                blogCatDao.create(bpc);
            }

        }
    }

    void addNewHashTags(List<String> oldTags, List<String> newTags, List<String> allTags, BlogPost blogPost) {
        for (String s : newTags) {

            if (oldTags.contains(s)) {

                continue;

            } else if (allTags.contains(s)) {

                BlogPostHashTags bpht = new BlogPostHashTags();
                HashTag ht = hashDao.returnByName(s);

                bpht.setBlogId(blogPost.getId());
                bpht.setHashTagId(ht.getId());

                blogHashDao.create(bpht);
            } else if (!(allTags.contains(s))) {

                HashTag tag = new HashTag();

                tag.setTag(s);

                HashTag newTag = hashDao.create(tag);

                BlogPostHashTags bpht = new BlogPostHashTags();

                bpht.setBlogId(blogPost.getId());
                bpht.setHashTagId(newTag.getId());

                blogHashDao.create(bpht);

            }

        }
    }

    List<String> getOldCategories(BlogPost blogPost) {
        List<BlogPostCategory> bpcList = blogCatDao.returnByBlogId(blogPost.getId());

        List<String> oldCategories = new ArrayList();

        for (BlogPostCategory bpc : bpcList) {

            Category c = categoryDao.read(bpc.getCategoryId());

            oldCategories.add(c.getCategoryName());

        }

        return oldCategories;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BlogPost delete(@PathVariable("id") Integer blogPostId) {

        BlogPost blogPost = blogDao.read(blogPostId);

        return blogPost;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void confirmDelete(@RequestBody BlogPost blogpost) {

        blogHashDao.delete(blogpost);

        blogCatDao.delete(blogpost);

        blogDao.delete(blogpost);

    }

}
