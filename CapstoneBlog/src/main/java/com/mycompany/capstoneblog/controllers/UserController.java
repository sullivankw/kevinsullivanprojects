/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.controllers;

import com.mycompany.capstoneblog.dao.AuthorityDao;
import com.mycompany.capstoneblog.dao.UserDao;
import com.mycompany.capstoneblog.dto.Authority;
import com.mycompany.capstoneblog.dto.User;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */

@Controller
@RequestMapping(value="/user")
public class UserController {
    
    private UserDao userDao;
    private AuthorityDao authorityDao;
    private PasswordEncoder encoder;
    
    @Inject
    public UserController(UserDao userDao, AuthorityDao authorityDao, PasswordEncoder encoder){
        
        this.userDao = userDao;
        this.authorityDao = authorityDao;
        this.encoder = encoder;
        
    }
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public User createUser(@RequestBody User user){
       
        String pass = user.getPassword();
        
        String hashPass = encoder.encode(pass);
        
        user.setPassword(hashPass);
        
        User u = userDao.create(user);
        
        Authority authority = new Authority();
        
        authority.setUserName(user.getUserName());
        authority.setAuthority(user.getRole());
        
        authorityDao.create(authority);
        
        return u;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User deleteUser(@PathVariable("id") Integer userId) {

        User u = userDao.read(userId);
        
        return u;

    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void confirmUserDelete(@RequestBody User user) {
        
        Authority authority = new Authority();
        
        
        authority.setUserName(user.getUserName());
        authority.setAuthority(user.getRole());
        
        authorityDao.deleteByUserName(authority);
        userDao.delete(user);

    }
    
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String updateUser(@PathVariable("id") Integer pageId, Map model) {
        
        User user = userDao.read(pageId);

        model.put("user", user);

        return "update";

    }
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public User updateSubmitUser(@Valid @RequestBody User user) {
        
        String pass = user.getPassword();
        
        String hashPass = encoder.encode(pass);
        
        user.setPassword(hashPass);

        Authority authority = new Authority();
        authority.setId(user.getId());
        authority.setUserName(user.getUserName());
        authority.setAuthority(user.getRole());
        authorityDao.update(authority);
        
        userDao.update(user);

        return user;
    }
    
}
