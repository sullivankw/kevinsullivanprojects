package com.mycompany.capstoneblog.controllers;

import com.mycompany.capstoneblog.dao.BlogPostDao;
import com.mycompany.capstoneblog.dao.StaticPageDao;
import com.mycompany.capstoneblog.dto.BlogPost;
import com.mycompany.capstoneblog.dto.StaticPage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Created by mymac on 10/10/16.
 */

@Controller
@RequestMapping(value = "/staticpage")
public class StaticPageController {

    private StaticPageDao staticPageDao;

    @Inject
    public StaticPageController(StaticPageDao staticPageDao) {

        this.staticPageDao = staticPageDao;

    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(Map model) {

        List<StaticPage> pages = staticPageDao.listAll();
        List<StaticPage> stashies = staticPageDao.listActivePages();

        model.put("staticPage", new StaticPage());
        model.put("staticPageList", pages);
        model.put("stashiesList", stashies);

        return "adminStaticPageHome";
    }

    @RequestMapping(value = "show/{id}", method = RequestMethod.GET)
    public String show(@PathVariable("id") Integer staticPageId, Map model) {


        StaticPage staticPage = staticPageDao.read(staticPageId);
        List<StaticPage> stashies = staticPageDao.listActivePages();

        model.put("page", staticPage);
        model.put("stashiesList", stashies);


        return "adminShowStaticPage";

    }

    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String getToUpdate(@PathVariable("id") Integer pageId, Map model) {

        StaticPage page = staticPageDao.read(pageId);

        model.put("page", page);

        return "adminStaticPageUpdate";

    }

    @RequestMapping(value = "update/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public StaticPage editSubmit(@RequestBody StaticPage staticPage) {

        staticPageDao.update(staticPage);

        return staticPage;

    }



    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public StaticPage create(@RequestBody StaticPage staticPage) {


        //staticPage.setActive(true);

        StaticPage s = staticPageDao.create(staticPage);

        return s;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public StaticPage delete(@PathVariable("id") Integer staticPostId) {

        StaticPage staticPage = staticPageDao.read(staticPostId);

        return staticPage;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void confirmDelete(@RequestBody StaticPage staticPage) {

        //blogHashDao.delete(blogpost);

        staticPageDao.delete(staticPage);

    }


    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String userGoToStaticPAges(Map model) {

        List<StaticPage> pages = staticPageDao.listActivePages();

        model.put("staticPage", new StaticPage());
        model.put("staticPageList", pages);

        return "userStaticPageHome";
    }

}
