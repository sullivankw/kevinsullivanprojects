package com.mycompany.capstoneblog.controllers;

import com.mycompany.capstoneblog.dao.*;
import com.mycompany.capstoneblog.dto.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Created by mymac on 10/19/16.
 */
@Controller
public class MarketerHomeController {

    private BlogPostDao blogPostDao;
    private HashTagDao hashTagDao;
    private UserDao userDao;
    private CategoryDao categoryDao;
    private StaticPageDao staticDao;


    @Inject
    public MarketerHomeController(StaticPageDao staticDao, CategoryDao categoryDao, BlogPostDao blogPostDao, HashTagDao hashTagDao, UserDao userDao) {
        this.blogPostDao = blogPostDao;
        this.hashTagDao = hashTagDao;
        this.userDao = userDao;
        this.categoryDao = categoryDao;
        this.staticDao = staticDao;

    }

    @RequestMapping(value = "/marketerhome", method = RequestMethod.GET)
    public String home(Map model) {


        List<BlogPost> blogPosts = blogPostDao.listAll();
        List<HashTag> hashies = hashTagDao.listAll();
        List<StaticPage> stashies = staticDao.listActivePages();
        List<Category> categories = categoryDao.listAll();

        List<BlogPost> blogPostsMarketerList = blogPostDao.listPendingPosts();

        model.put("blog", new BlogPost());
        model.put("blogPostList", blogPosts);
        model.put("user", new User());
        model.put("categories", categories);
        model.put("stashiesList", stashies);
        model.put("marketList", blogPostsMarketerList);

        return "marketerHome";
    }

}


