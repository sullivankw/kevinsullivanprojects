package com.mycompany.capstoneblog.controllers;

import com.mycompany.capstoneblog.dao.BlogPostCategoryDao;
import com.mycompany.capstoneblog.dao.BlogPostDao;
import com.mycompany.capstoneblog.dao.StaticPageDao;
import com.mycompany.capstoneblog.dto.BlogPost;
import com.mycompany.capstoneblog.dto.StaticPage;
import com.mycompany.capstoneblog.dao.CategoryDao;
import com.mycompany.capstoneblog.dto.BlogPostCategory;
import com.mycompany.capstoneblog.dto.Category;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by mymac on 10/12/16.
 */
@Controller
public class UserHomeController {

    private BlogPostDao blogPostDao;
    private StaticPageDao staticPageDao;
    private CategoryDao categoryDao;
    private BlogPostCategoryDao bpcDao;

    @Inject
    public UserHomeController(BlogPostDao blogPostDao, CategoryDao categoryDao, BlogPostCategoryDao bpcDao, StaticPageDao staticPageDao) {
        this.blogPostDao = blogPostDao;
        this.categoryDao = categoryDao;
        this.bpcDao = bpcDao;
        this.staticPageDao = staticPageDao;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Map model) {

      List<BlogPost> blogPosts = blogPostDao.listActivePosts();
       List<StaticPage> stashies = staticPageDao.listActivePages();
        List<Category> categories = categoryDao.listAll();

        model.put("blog", new BlogPost());
        model.put("blogPostList", blogPosts);
        model.put("categoryList", categories);
        model.put("stashiesList", stashies);

        return "userHome";
    }

    @RequestMapping(value = "/home/submitform", method = RequestMethod.GET)
    public String sendFile(Map model) {
        List<StaticPage> stashies = staticPageDao.listActivePages();
        model.put("stashiesList", stashies);



        return "userSubmitFile";
    }

    @RequestMapping(value = "/categories/{id}", method = RequestMethod.GET)
    public String showPostByCategories(@PathVariable("id") Integer id, Map model) {

        List<BlogPostCategory> bpcList = bpcDao.returnByCategoryId(id);
        List<BlogPost> bpList = new ArrayList();

        for (BlogPostCategory bpc : bpcList) {

            BlogPost blog = blogPostDao.read(bpc.getBlogId());

            bpList.add(blog);

        }
        
        model.put("bpList", bpList);
        
        return "showBlogByCategory";

    }

}
