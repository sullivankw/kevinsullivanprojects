package com.mycompany.capstoneblog.aop;

/**
 * Created by mymac on 9/23/16.
 */
public class ValidationError {

    private String fieldName;
    private String message;


    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



}
