package com.mycompany.capstoneblog.aop;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

/**
 * Created by mymac on 9/23/16.
 */
@ControllerAdvice
//called after every controller method
public class RestValidationHandler {


    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorContainer processValidationErrors(MethodArgumentNotValidException ex) {

        BindingResult result = ex.getBindingResult();

        //here we grab em so we can get em in a container to serlialize back to cloent
        List<FieldError> fieldErrors = result.getFieldErrors();

        ValidationErrorContainer container = new ValidationErrorContainer();

        for (FieldError error : fieldErrors) {

            ValidationError vError = new ValidationError();
            vError.setFieldName(error.getField());
            vError.setMessage(error.getDefaultMessage());
            container.addError(vError);
        }

        return container;

    }


}