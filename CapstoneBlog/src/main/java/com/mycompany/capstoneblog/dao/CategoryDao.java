/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.Category;
import com.mycompany.capstoneblog.dto.HashTag;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface CategoryDao {

    Category create(Category category);

    Category read(Integer id);

    void update(Category category);

    void delete(Category category);

    List<Category> listAll();

    public Category returnByName(String name);

}
