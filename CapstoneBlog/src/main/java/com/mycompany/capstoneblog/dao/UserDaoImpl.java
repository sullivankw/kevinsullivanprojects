package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mymac on 10/5/16.
 */
public class UserDaoImpl implements  UserDao {

    private static final String SQL_UPDATE_USER = "UPDATE user SET user_name=?, password=?, role=?, enabled=? WHERE id=?";
    private static final String SQL_SELECT_USER = "SELECT * FROM user WHERE id=?";
    private static final String SQL_DELETE_USER = "DELETE FROM user WHERE id=?";
    private static final String SQL_CREATE_USER = "INSERT INTO user (user_name, password, role, enabled) VALUES (?, ?, ?, ?)";
    private static final String SQL_SELECT_ALL_USERS = "SELECT * FROM user";


    private JdbcTemplate jdbcTemplate;

    public UserDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User create(User user) {
        jdbcTemplate.update(SQL_CREATE_USER, user.getUserName(), user.getPassword(), user.getRole(), user.getEnabled());
        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        user.setId(newId);
        return user;
    }

    @Override
    public User read(Integer id) {
        User a = jdbcTemplate.queryForObject(SQL_SELECT_USER, new UserMapper(), id);
        return a;
    }

    @Override
    public void update(User user) {
        jdbcTemplate.update(SQL_UPDATE_USER, user.getUserName(), user.getPassword(), user.getRole(), user.getEnabled(), user.getId());

    }

    @Override
    public void delete(User user) {
        
        jdbcTemplate.update(SQL_DELETE_USER, user.getId());

    }

    @Override
    public List<User> listAll() {
        List<User> users = jdbcTemplate.query(SQL_SELECT_ALL_USERS, new UserMapper());
        return users;
    }

    private static final class UserMapper implements org.springframework.jdbc.core.RowMapper<User> {

        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {

            User a = new User();
            a.setId(resultSet.getInt("id"));
            a.setUserName(resultSet.getString("user_name"));
            a.setPassword(resultSet.getString("password"));
            a.setRole(resultSet.getString("role"));
            a.setEnabled(resultSet.getInt("enabled"));
            

            return a;

        }
    }



}
