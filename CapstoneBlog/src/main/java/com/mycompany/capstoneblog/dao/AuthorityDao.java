/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.Authority;
import com.mycompany.capstoneblog.dto.User;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface AuthorityDao {
    public Authority create(Authority authority);

    public Authority read(Integer id);

    public void update(Authority authority);

    public void delete(Authority authority);
    
    public void deleteByUserName(Authority authority);

    public List<Authority> listAll();
}
