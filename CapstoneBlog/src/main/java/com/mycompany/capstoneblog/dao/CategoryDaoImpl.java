package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.Category;
import com.mycompany.capstoneblog.dto.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by mymac on 10/5/16.
 */
public class CategoryDaoImpl implements CategoryDao {

    private static final String SQL_UPDATE_CATEGORY = "UPDATE categories SET category_name=? WHERE id=?";
    private static final String SQL_SELECT_CATEGORY = "SELECT * FROM categories WHERE id=?";
    private static final String SQL_DELETE_CATEGORY = "DELETE FROM categories WHERE id=?";
    private static final String SQL_CREATE_CATEGORY = "INSERT INTO categories (category_name) VALUES (?)";
    private static final String SQL_SELECT_ALL_CATEGORY = "SELECT * FROM categories";
    private static final String SQL_SELECT_BY_NAME = "SELECT * FROM categories WHERE category_name = ?";
    
    private JdbcTemplate jdbcTemplate;

    public CategoryDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Category create(Category category) {
        jdbcTemplate.update(SQL_CREATE_CATEGORY, category.getCategoryName());
        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        category.setId(newId);
        return category;
    }

    @Override
    public Category read(Integer id) {
        Category a = jdbcTemplate.queryForObject(SQL_SELECT_CATEGORY, new CategoryDaoImpl.CategoryMapper(), id);
        return a;

    }

    @Override
    public void update(Category category) {
        jdbcTemplate.update(SQL_UPDATE_CATEGORY, category.getCategoryName(), category.getId());

    }

    @Override
    public void delete(Category category) {
        jdbcTemplate.update(SQL_DELETE_CATEGORY, category.getId());

    }

    @Override
    public List<Category> listAll() {
        List<Category> categories = jdbcTemplate.query(SQL_SELECT_ALL_CATEGORY, new CategoryDaoImpl.CategoryMapper());
        return categories;
    }

    @Override
    public Category returnByName(String name) {

        Category cat = jdbcTemplate.queryForObject(SQL_SELECT_BY_NAME, new CategoryMapper(), name);
        return cat;

    }

    private static final class CategoryMapper implements org.springframework.jdbc.core.RowMapper<Category> {

        @Override
        public Category mapRow(ResultSet resultSet, int i) throws SQLException {

            Category a = new Category();
            a.setId(resultSet.getInt("id"));
            a.setCategoryName(resultSet.getString("category_name"));

            return a;

        }
    }

}
