/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.BlogPost;
import com.mycompany.capstoneblog.dto.BlogPostHashTags;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogPostHashTagsDao {
    
    BlogPostHashTags create(BlogPostHashTags bpht);
    List<BlogPostHashTags> read(Integer id);
    void update(BlogPostHashTags bpht);
    void delete(BlogPost blogPost);
    List<BlogPostHashTags> listAll();
    List<BlogPostHashTags> returnByHashTagId(Integer id);
    void deleteById(Integer id);
    
}
