/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.HashTag;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class HashTagDaoImpl implements HashTagDao {

    private static final String SQL_INSERT_HASHTAG = "INSERT INTO `hash_tag` (`tag`) VALUES (?)";
    private static final String SQL_DELETE_HASHTAG = "DELETE FROM `hash_tag` WHERE id = ? ";
    private static final String SQL_SELECT_HASHTAG = "SELECT * FROM hash_tag WHERE id=?";
    private static final String SQL_UPDATE_HASHTAG = "UPDATE `hash_tag` SET tag=? WHERE `id`=?";
    private static final String SQL_SELECT_ALL_HASHTAG = "SELECT * FROM hash_tag";
    private static final String SQL_SELECT_BY_NAME = "SELECT * FROM hash_tag WHERE tag = ?";
    
    private JdbcTemplate jdbcTemplate;

    public HashTagDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public HashTag create(HashTag hashTag) {
        
        jdbcTemplate.update(SQL_INSERT_HASHTAG,
                hashTag.getTag()
               
        );

        Integer hashTagId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        hashTag.setId(hashTagId);

        return hashTag;
    }

    @Override
    public HashTag read(Integer id) {

        HashTag hashTag = jdbcTemplate.queryForObject(SQL_SELECT_HASHTAG, new ContactMapper(), id);
        return hashTag;
        
    }

    @Override
    public void update(HashTag hashTag) {

         jdbcTemplate.update(SQL_UPDATE_HASHTAG,
                hashTag.getTag(),
                hashTag.getId());

    }

    @Override
    public void delete(HashTag hashTag) {

        jdbcTemplate.update(SQL_DELETE_HASHTAG, hashTag.getId());

    }

    @Override
    public List<HashTag> listAll() {

        List<HashTag> hashTag = jdbcTemplate.query(SQL_SELECT_ALL_HASHTAG, new ContactMapper());
        return hashTag;

    }
    
    public HashTag returnByName(String name){
        
        HashTag hashTag = jdbcTemplate.queryForObject(SQL_SELECT_BY_NAME, new ContactMapper(), name);
        return hashTag;
        
    }
    
    private static final class ContactMapper implements RowMapper<HashTag> {
    @Override
        public HashTag mapRow(ResultSet rs, int i) throws SQLException {
            
            HashTag hashTag = new HashTag();
            hashTag.setId(rs.getInt("id"));
            hashTag.setTag(rs.getString("tag"));
            
            return hashTag;

        }
    }

}
