/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.BlogPost;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogPostDao {
    
    BlogPost create(BlogPost blogPost);
    BlogPost read(Integer id);
    void update(BlogPost blogPost);
    void delete(BlogPost blogPost);
    List<BlogPost> listAll();
    List<BlogPost> listActivePosts();
    List<BlogPost> listPendingPosts();
    
}
