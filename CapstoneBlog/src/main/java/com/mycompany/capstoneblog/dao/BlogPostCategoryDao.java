/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.BlogPost;
import com.mycompany.capstoneblog.dto.BlogPostCategory;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogPostCategoryDao {
    
    BlogPostCategory create(BlogPostCategory b);
    BlogPostCategory read(Integer id);
    void update(BlogPostCategory b);
    void deleteById(Integer id);
    void delete(BlogPost blogPost);
    List<BlogPostCategory> listAll();
    List<BlogPostCategory> returnByCategoryId(Integer id);
    List<BlogPostCategory> returnByBlogId(Integer id);
    
}
