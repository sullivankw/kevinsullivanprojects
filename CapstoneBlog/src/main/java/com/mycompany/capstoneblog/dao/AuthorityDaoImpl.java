/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.Authority;
import com.mycompany.capstoneblog.dto.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AuthorityDaoImpl implements AuthorityDao {

    private static final String SQL_UPDATE_AUTHORITIES = "UPDATE authority SET username=?, authority=? WHERE id=?";
    private static final String SQL_SELECT_AUTHORITIES = "SELECT * FROM authority WHERE id=?";
    private static final String SQL_DELETE_AUTHORITIES = "DELETE FROM authority WHERE id=?";
    private static final String SQL_DELETE_AUTHORITIES_BY_USERNAME = "DELETE FROM authority WHERE username=?";
    private static final String SQL_CREATE_AUTHORITIES = "INSERT INTO authority (username, authority) VALUES (?, ?)";
    private static final String SQL_SELECT_ALL_AUTHORITIES = "SELECT * FROM authority";

    private JdbcTemplate jdbcTemplate;

    public AuthorityDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Authority create(Authority authority) {

        jdbcTemplate.update(SQL_CREATE_AUTHORITIES, authority.getUserName(), authority.getAuthority());
        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        authority.setId(newId);
        return authority;

    }

    @Override
    public Authority read(Integer id) {

        Authority a = jdbcTemplate.queryForObject(SQL_SELECT_AUTHORITIES, new AuthorityMapper(), id);
        return a;

    }

    @Override
    public void update(Authority authority) {

        jdbcTemplate.update(SQL_UPDATE_AUTHORITIES, authority.getUserName(), authority.getAuthority(), authority.getId());

    }

    @Override
    public void delete(Authority authority) {

        jdbcTemplate.update(SQL_DELETE_AUTHORITIES, authority.getId());

    }

    @Override
    public List<Authority> listAll() {

        List<Authority> authority = jdbcTemplate.query(SQL_SELECT_ALL_AUTHORITIES, new AuthorityMapper());
        return authority;

    }

    @Override
    public void deleteByUserName(Authority authority) {

           jdbcTemplate.update(SQL_DELETE_AUTHORITIES_BY_USERNAME, authority.getUserName());

    }

    private static final class AuthorityMapper implements org.springframework.jdbc.core.RowMapper<Authority> {

        @Override
        public Authority mapRow(ResultSet resultSet, int i) throws SQLException {

            Authority a = new Authority();
            a.setId(resultSet.getInt("id"));
            a.setUserName(resultSet.getString("username"));
            a.setAuthority(resultSet.getString("authority"));
            

            return a;

        }
    }
    
}
