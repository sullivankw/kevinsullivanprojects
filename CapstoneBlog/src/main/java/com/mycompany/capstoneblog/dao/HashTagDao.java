/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.HashTag;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface HashTagDao {

    public HashTag create(HashTag hashTag);

    public HashTag read(Integer id);

    public void update(HashTag hashTag);

    public void delete(HashTag hashTag);

    public List<HashTag> listAll();
    
    public HashTag returnByName(String name);

}
