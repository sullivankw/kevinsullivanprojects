/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.BlogPost;
import com.mycompany.capstoneblog.dto.BlogPostHashTags;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class BlogPostHashTagsDaoImpl implements BlogPostHashTagsDao {

    private static final String SQL_CREATE_BPHT = "INSERT INTO blog_hashtags (blog_id, hashtag_id) VALUES (?, ?)";
    private static final String SQL_SELECT_BPHT = "SELECT * FROM blog_hashtags WHERE blog_id = ?";
    private static final String SQL_DELETE_BPHT = "DELETE FROM blog_hashtags WHERE blog_id = ?";
    private static final String SQL_UPDATE_BPHT = "UPDATE blog_hashtags SET blog_id = ?, hashtag_id = ? WHERE id = ?";
    private static final String SQL_SELECT_ALL_BPHT = "SELECT * FROM blog_hashtags";
    private static final String SQL_SELECT_BY_HASH_ID = "SELECT * FROM blog_hashtags WHERE hashtag_id = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM blog_hashtags WHERE id = ?";

    private JdbcTemplate jdbcTemplate;

    public BlogPostHashTagsDaoImpl(JdbcTemplate jdbcTemplate) {

        this.jdbcTemplate = jdbcTemplate;

    }

    @Override
    public BlogPostHashTags create(BlogPostHashTags bpht) {

        jdbcTemplate.update(SQL_CREATE_BPHT, bpht.getBlogId(), bpht.getHashTagId());

        return bpht;

    }

    @Override
    public List<BlogPostHashTags> read(Integer id) {

        List<BlogPostHashTags> b = jdbcTemplate.query(SQL_SELECT_BPHT, new BlogPostHashTagsMapper(), id);

        return b;
    }

    @Override
    public void update(BlogPostHashTags bpht) {

        jdbcTemplate.update(SQL_UPDATE_BPHT,
                bpht.getBlogId(),
                bpht.getHashTagId());

    }

    @Override
    public void delete(BlogPost blogPost) {

        jdbcTemplate.update(SQL_DELETE_BPHT, blogPost.getId());

    }

    @Override
    public List<BlogPostHashTags> listAll() {

        List<BlogPostHashTags> bpht = jdbcTemplate.query(SQL_SELECT_ALL_BPHT, new BlogPostHashTagsMapper());
        return bpht;

    }

    @Override
    public List<BlogPostHashTags> returnByHashTagId(Integer id) {

        List<BlogPostHashTags> bpht = jdbcTemplate.query(SQL_SELECT_BY_HASH_ID, new BlogPostHashTagsMapper(), id);
        return bpht;

    }

    @Override
    public void deleteById(Integer id) {

        jdbcTemplate.update(SQL_DELETE_BY_ID, id);

    }

    private static final class BlogPostHashTagsMapper implements RowMapper<BlogPostHashTags> {

        @Override
        public BlogPostHashTags mapRow(ResultSet rs, int i) throws SQLException {

            BlogPostHashTags bpht = new BlogPostHashTags();

            bpht.setId(rs.getInt("id"));
            bpht.setBlogId(rs.getInt("blog_id"));
            bpht.setHashTagId(rs.getInt("hashtag_id"));

            return bpht;

        }
    }

}
