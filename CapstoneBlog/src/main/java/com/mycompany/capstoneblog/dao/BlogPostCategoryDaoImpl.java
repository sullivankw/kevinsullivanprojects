/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.capstoneblog.dao;

import com.mycompany.capstoneblog.dto.BlogPost;
import com.mycompany.capstoneblog.dto.BlogPostCategory;
import com.mycompany.capstoneblog.dto.BlogPostHashTags;
import com.mycompany.capstoneblog.dto.Category;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class BlogPostCategoryDaoImpl implements BlogPostCategoryDao {

    private static final String SQL_CREATE_BPC = "INSERT INTO blog_categories (blog_id, category_id) VALUES (?, ?)";
    private static final String SQL_SELECT_BPC = "SELECT * FROM blog_categories WHERE category_id = ?";
    private static final String SQL_DELETE_BPC = "DELETE FROM blog_categories WHERE blog_id = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM blog_categories WHERE id = ?";
    private static final String SQL_UPDATE_BPC = "UPDATE blog_categories SET blog_id = ?, category_id = ? WHERE id = ?";
    private static final String SQL_SELECT_ALL_BPC = "SELECT * FROM blog_categories";
    private static final String SQL_SELECT_BY_CAT_ID = "SELECT * FROM blog_categories WHERE category_id = ?";
    private static final String SQL_SELECT_BY_BLOG_ID = "SELECT * FROM blog_categories WHERE blog_id = ?";

    private JdbcTemplate jdbcTemplate;

    public BlogPostCategoryDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public BlogPostCategory create(BlogPostCategory b) {

        jdbcTemplate.update(SQL_CREATE_BPC, b.getBlogId(), b.getCategoryId());

        return b;
    }

    @Override
    public BlogPostCategory read(Integer id) {

        BlogPostCategory bpc = jdbcTemplate.queryForObject(SQL_SELECT_BPC, new BlogPostCategoryMapper(), id);
        return bpc;

    }

    @Override
    public void update(BlogPostCategory b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(BlogPost blogPost) {
        jdbcTemplate.update(SQL_DELETE_BPC, blogPost.getId());

    }

    @Override
    public void deleteById(Integer id) {

        jdbcTemplate.update(SQL_DELETE_BY_ID, id);

    }

    @Override
    public List<BlogPostCategory> listAll() {

        List<BlogPostCategory> b = jdbcTemplate.query(SQL_SELECT_ALL_BPC, new BlogPostCategoryMapper());
        return b;

    }

    @Override
    public List<BlogPostCategory> returnByCategoryId(Integer id) {
        List<BlogPostCategory> b = jdbcTemplate.query(SQL_SELECT_BY_CAT_ID, new BlogPostCategoryMapper(), id);
        return b;
    }

    @Override
    public List<BlogPostCategory> returnByBlogId(Integer id) {

        List<BlogPostCategory> b = jdbcTemplate.query(SQL_SELECT_BY_BLOG_ID, new BlogPostCategoryMapper(), id);
        return b;

    }

    private static final class BlogPostCategoryMapper implements RowMapper<BlogPostCategory> {

        @Override
        public BlogPostCategory mapRow(ResultSet rs, int i) throws SQLException {

            BlogPostCategory bpc = new BlogPostCategory();

            bpc.setId(rs.getInt("id"));
            bpc.setBlogId(rs.getInt("blog_id"));
            bpc.setCategoryId(rs.getInt("category_id"));

            return bpc;

        }
    }

}
