/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui.theconsoleio;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author mymac
 */
public class EnhancedConsoleIO {

    public int getInt(String prompt) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        int userInt = 0;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();

                userInt = Integer.parseInt(input);

                valid = true;

            } catch (NumberFormatException e) {
                System.out.println("Enter an integer: ");
            }

        }
        return userInt;
    }

    public int getIntRange(String prompt, int max, int min) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        int userInt = 0;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();

                userInt = Integer.parseInt(input);

                if (userInt > max) {
                    System.out.println("Enter an integer less than or equal to " + max + ": ");
                } else if (userInt < min) {
                    System.out.println("Enter an integer greater than or equal to " + min + ": ");
                } else {
                    valid = true;
                }

            } catch (NumberFormatException e) {
                System.out.println("Enter an integer: ");
            }

        }
        return userInt;
    }

    public String getString(String prompt) {
        Scanner sc = new Scanner(System.in);
        System.out.print(prompt);
        String userString = sc.nextLine();
        return userString;
    }

    public String getStringTeamRestrictions(String prompt) {
        Scanner sc = new Scanner(System.in);
        System.out.print(prompt);
        boolean answerPosition = true;
        String input = "";
        while (answerPosition) {
            String userString = sc.nextLine();
            input = userString;

            switch (input) {
                case "Cats":
                    answerPosition = false;
                    break;
                case "Hats":
                    answerPosition = false;
                    break;
                case "Bats":
                    answerPosition = false;
                    break;
                default:
                    System.out.println("Choose a valid team. We only got three....and don't waste your time with 'the'!");
                    break;

            }

        }
        return input;

    }


    public String getStringRestrictions(String prompt) {
        Scanner sc = new Scanner(System.in);
        System.out.print(prompt);
        boolean answerPosition = true;
        String input = "";
        while (answerPosition) {
            String userString = sc.nextLine();
            input = userString;

            switch (input) {
                case "1b":
                    answerPosition = false;
                    break;
                case "2b":
                    answerPosition = false;
                    break;
                case "3b":
                    answerPosition = false;
                    break;
                case "lf":
                    answerPosition = false;
                    break;
                case "rf":
                    answerPosition = false;
                    break;
                case "cf":
                    answerPosition = false;
                    break;
                case "ss":
                    answerPosition = false;
                    break;
                case "p":
                    answerPosition = false;
                    break;
                case "c":
                    answerPosition = false;
                    break;
                default:
                    System.out.println("Choose a valid position");
                    break;

            }

        }
        return input;

    }


    public float getFloat(String prompt) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        float userFloat = 0;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();

                userFloat = Float.parseFloat(input);

                valid = true;

            } catch (NumberFormatException e) {
                System.out.println("Enter a float: ");
            }

        }
        return userFloat;
    }

    public float getFloatRange(String prompt, float max, float min) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        float userFloat = 0;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();

                userFloat = Float.parseFloat(input);

                if (userFloat > max) {
                    System.out.println("Enter a float less than or equal to " + max + ": ");
                } else if (userFloat < min) {
                    System.out.println("Enter a float greater than or equal to " + min + ": ");
                } else {
                    valid = true;
                }

            } catch (NumberFormatException e) {
                System.out.println("Enter a float: ");
            }

        }
        return userFloat;
    }

    public double getDouble(String prompt) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        double userDouble = 0;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();

                userDouble = Double.parseDouble(input);

                valid = true;

            } catch (NumberFormatException e) {
                System.out.println("Enter a double: ");
            }

        }
        return userDouble;
    }

    public double getDoubleRange(String prompt, double max, double min) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        double userDouble = 0;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();

                userDouble = Double.parseDouble(input);

                if (userDouble > max) {
                    System.out.println("Enter a double less than or equal to " + max + ": ");
                } else if (userDouble < min) {
                    System.out.println("Enter a double greater than or equal to " + min + ": ");
                } else {
                    valid = true;
                }

            } catch (NumberFormatException e) {
                System.out.println("Enter a double: ");
            }

        }
        return userDouble;
    }

    public void displayString(String prompt) {

        System.out.println(prompt);

    }

    public String getStringNoCommas(String prompt) {
        Scanner sc = new Scanner(System.in);
        System.out.print(prompt);
        String userString = sc.nextLine();
        String cow = "";

            if (userString.contains(",")) {
                cow = userString.replace(",", "_");

            }

        return cow;
    }


    public Double getDoubleLeaveBlank(String prompt, Double notNull) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        Double userDouble = 0.00;
        Double modifiedUserDouble = notNull;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();

                userDouble = Double.parseDouble(input);

                if (userDouble <= 0) {
                    System.out.println("You should  enter a number larger than zero");
                    System.out.println("The value will stay the same");
                } else if (userDouble > 0) {
                    modifiedUserDouble = userDouble;
                break;
            } else {
                    valid = true;
            }

            } catch (NumberFormatException e) {
                //if ()
                //System.out.println("Enter a double: ");
                System.out.println("The value will stay the same. ");
                modifiedUserDouble = notNull;
                break;
                //valid = true;
            }

        }
            return modifiedUserDouble;

    }


    public String getDateFormattedMMddYYYY(String prompt) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        String modifiedUserString = "";

        System.out.println(prompt);


        while (!valid)
        {

        try {
            String userDate = sc.nextLine();
            LocalDate newDate = LocalDate.parse(userDate);

            DateTimeFormatter formatter;
            formatter = DateTimeFormatter.ofPattern("MMddyyyy");

            modifiedUserString = newDate.format(formatter).toString();
            System.out.println(modifiedUserString);
            valid = true;

        } catch (Exception e) {
            //e.System.out.println("Enter a valid date in YYYY-MM-DD format. ");
            System.out.println("Enter a valid date in YYYY-MM-DD format. ");
        }

        }
        return modifiedUserString;
    }

    public String getDateFormattedMMddYYYYForEdit(String prompt, String pastDate) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        String modifiedUserString = "";
        //String modifiedModifiedUserString ="";
        //boolean go = true;

        System.out.println(prompt);
        String userDate = sc.nextLine();

                if (userDate.equals("")) {
                    modifiedUserString  = pastDate;
                    valid = true;

                 }


        while (!valid)
        {

            try {
                userDate = sc.nextLine();
                LocalDate newDate = LocalDate.parse(userDate);

                DateTimeFormatter formatter;
                formatter = DateTimeFormatter.ofPattern("MMddyyyy");

                modifiedUserString = newDate.format(formatter).toString();
                System.out.println(modifiedUserString);
                valid = true;

            } catch (Exception e) {
                //e.System.out.println("Enter a valid date in YYYY-MM-DD format. ");
                System.out.println("Enter a valid date in YYYY-MM-DD format. ");
            }

        }
        return modifiedUserString;
    }


}

    

