package com.mycompany.app;

import com.mycompany.controller.FlooringController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by mymac on 8/30/16.
 */
public class App {

    public static void main(String[] arg) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        FlooringController fc = (FlooringController) ctx.getBean("flooringcontroller");

        fc.run();

    }
}

