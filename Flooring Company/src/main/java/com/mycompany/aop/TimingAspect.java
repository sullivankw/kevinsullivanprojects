package com.mycompany.aop;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Created by mymac on 9/10/16.
 */
public class TimingAspect {

    public Object timeMethod(ProceedingJoinPoint jp) {

        Object ret = null;

        try {

            long start = System.currentTimeMillis();

            ret = jp.proceed();

            long end = System.currentTimeMillis();
            System.out.println(jp.getSignature().getName() + " took "
                    + (end- start) + " ms.");


        } catch (Throwable ex) {
            System.out.println("Exception in TimerAspect.timeMethod()");

        }

        return ret;
    }


}
