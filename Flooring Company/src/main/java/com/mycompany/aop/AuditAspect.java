package com.mycompany.aop;

import com.mycompany.dao.AuditDao;
import com.mycompany.dto.Audit;
import com.mycompany.dto.Order;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by mymac on 9/8/16.
 */
public class AuditAspect {



public AuditAspect() {}



    private List<Audit> audits;

    private static final String TOKEN = ",";


    public void auditTest(JoinPoint jp) {



        Order s = (Order) jp.getArgs()[1];

        Audit a = new Audit();

        try {


        a.setOperation(jp.getSignature().getName());

        a.setDate(LocalDate.now().toString());
        a.setTime(LocalTime.now().toString());
        a.setOperationOrderNumber(s.getOrderNumber());
            a.setDateOfOperation(s.getOrderDate());

        add(a);
    }catch (Throwable ex) {
            System.out.println("Exception in AuditAspect.auditMethod()");

    }



    }

    public void encode() {


        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter("AUDIT.txt"));

            for (Audit s : audits) {

                out.print(s.getId());
                out.print(TOKEN);

                out.print(s.getOperation());
                out.print(TOKEN);

                out.print(s.getDate());
                out.print(TOKEN);

                out.print(s.getTime());
                out.print(TOKEN);

                out.print(s.getOperationOrderNumber());
                out.print(TOKEN);

                out.print(s.getDateOfOperation());
                out.print("\n");

            }

            out.flush();


        } catch (IOException ex) {


        } finally {

            out.close();
        }

    }

    public List<Audit> decode() {

        List<Audit> tempAuditList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("AUDIT.txt")));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);

                Audit a = new Audit();
                a.setId(Integer.parseInt(stringParts[0]));
                a.setOperation(stringParts[1]);
                a.setDate(stringParts[2]);
                a.setTime(stringParts[3]);
                a.setOperationOrderNumber(Integer.parseInt(stringParts[4]));
                a.setDateOfOperation(stringParts[5]);

                tempAuditList.add(a);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(AuditDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tempAuditList;
    }

    public Audit add(Audit audit) {

        audits = decode();

        audit.setId(audits.size() + 1);

        audits.add(audit);

        encode();

        return audit;

    }

    public void createTextFile(File file) {


        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(file));

            out.flush();

        } catch (IOException ex) {

        } finally {

            out.close();
        }

    }
}


