package com.mycompany.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mymac on 8/30/16.
 */
public class Tax {


    private int id;
    private String state;
    private double taxRate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }


    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public void setState(String state) {
        this.state = state;
    }



}
