package com.mycompany.dto;

import java.time.LocalDate;
import java.util.Calendar;

/**
 * Created by mymac on 9/8/16.
 */
public class Audit {
    private int id;
    private String operation;
    private String date;
    private int operationOrderNumber;
    //String dateExact;
    private String time;
    private String dateOfOperation;

    public String getDateOfOperation() {
        return dateOfOperation;
    }

    public void setDateOfOperation(String dateOfOperation) {
        this.dateOfOperation = dateOfOperation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;

    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public int getOperationOrderNumber() {
        return operationOrderNumber;
    }

    public void setOperationOrderNumber(int operationOrderNumber) {
        this.operationOrderNumber = operationOrderNumber;
    }


}
