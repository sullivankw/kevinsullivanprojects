package com.mycompany.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mymac on 8/30/16.
 */
public class Product {

    private int id;
    private String productType;
    private double costPerSquareFeet;
    private double laborCostPerSquareFeet;
    private List<Product> products;

    public Product(int id, String productType, double costPerSquareFeet, double laborCostPerSquareFeet) {
        this.id = id;
        this.productType = productType;
        this.costPerSquareFeet = costPerSquareFeet;
        this.laborCostPerSquareFeet = laborCostPerSquareFeet;

    }

    public Product() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public double getCostPerSquareFeet() {
        return costPerSquareFeet;
    }

    public void setCostPerSquareFeet(double costPerSquareFeet) {
        this.costPerSquareFeet = costPerSquareFeet;
    }

    public double getLaborCostPerSquareFeet() {
        return laborCostPerSquareFeet;
    }

    public void setLaborCostPerSquareFeet(double laborCostPerSquareFeet) {
        this.laborCostPerSquareFeet = laborCostPerSquareFeet;
    }
}
