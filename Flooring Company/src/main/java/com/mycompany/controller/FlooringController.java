package com.mycompany.controller;

import com.mycompany.dao.*;
import com.mycompany.dto.Order;
import com.mycompany.dto.Product;
import com.mycompany.dto.Tax;
import com.mycompany.ui.NonDepIO;


//import com.sun.java.util.jar.pack.Instruction;

import java.util.*;

/**
 * Created by mymac on 8/30/16.
 */
public class FlooringController {


    int taxid;
    int productid;

    private NonDepIO nio = new NonDepIO();
    private OrderDao orderDao;
    private TaxDao taxDao;
    private ProductDao productDao;

    public FlooringController(OrderDao orderDao, ProductDao productDao, TaxDao taxDao) {
        //this.orderDao = orderDao;
        this.orderDao = orderDao;
        this.productDao = productDao;
        this.taxDao = taxDao;

    }

    public void run() {

        boolean playAgain = true;

        while (playAgain) {
            nio.displayString("\n");
            nio.displayString("*************************SWC Ordering Center************************** ");
            nio.displayString("\n");
            nio.displayString("1. Display Orders ");
            nio.displayString("\n");
            nio.displayString("2. Add an Order ");
            nio.displayString("\n");
            nio.displayString("3. Edit an order ");
            nio.displayString("\n");
            nio.displayString("4. Delete an order ");
            nio.displayString("\n");
            nio.displayString("5. View tax list ");
            nio.displayString("\n");
            nio.displayString("6. View product list ");
            nio.displayString("\n");
            nio.displayString("7. Exit program ");
            nio.displayString("\n");
            int choice = nio.getIntRange("Please select an option ", 7, 1);
            nio.displayString("\n");

            switch (choice) {
                case 1:
                    confirmOrderDetails();
                    break;
                case 2:
                    add();
                    break;
                case 3:
                    edit();
                    break;
                case 4:
                    remove();
                    break;
                case 5:
                    listTaxes();
                    break;
                case 6:
                    listProducts();
                    break;
                case 7:
                    playAgain = false;

            }

        }

    }

    public Product setProduct() {
        int userProductId = nio.getIntRange("What material: 1.Carpet 2.Laminate 3.Tile 4.Wood? ", 4, 1);

        Product j = new Product();
        j.setId(userProductId);
        Product stuff = productDao.add(j);
        return stuff;

    }

    public Tax setTax() {
        int userTaxId = nio.getIntRange("Which state: 1.OH 2.PA 3.MI 4.IN? ", 4, 1);

        Tax tax = new Tax();
        tax.setId(userTaxId);
        Tax thing = taxDao.add(tax);

        return thing;

    }

    public void setAndMakeCalculations(Product stuff, Tax thing, double area, String lastName, Order o) {

        o.setProductType(stuff.getProductType());
        nio.displayString("In set and make: " + stuff.getProductType() );
        o.setLaborCostPerSquareFoot(stuff.getLaborCostPerSquareFeet());
        o.setCostPerSquareFoot(stuff.getCostPerSquareFeet());
        o.setState(thing.getState());
        o.setTaxRate(thing.getTaxRate());
        nio.displayString("In set and make: " + thing.getState() );
        o.setCustomerName(lastName);
        o.setArea(area);
        o.setTotal( ((area * stuff.getLaborCostPerSquareFeet()) + (area * stuff.getCostPerSquareFeet()) +
                ( (((area * stuff.getLaborCostPerSquareFeet()) + (area * stuff.getCostPerSquareFeet()) ) * (thing.getTaxRate()/100)) )) );
        o.setMaterialCost(area * stuff.getCostPerSquareFeet());
        o.setLaborCost(area * stuff.getLaborCostPerSquareFeet());
        o.setTax( (thing.getTaxRate()/100) * o.getTotal());
    }

    public boolean makeItHappen(Order o) {
        boolean shoe;
        String confirm = nio.getString("Would you like to continue? y/n: ");
        if ("yes".equalsIgnoreCase(confirm) || ("y".equalsIgnoreCase(confirm))) {
            nio.displayString("Order for " + o.getCustomerName() + " has been fixed. ");
            nio.displayString("-------------------------------------------------");
            shoe = true;
        } else {
            nio.displayString("Changes aborted. ");
            nio.displayString("-----------------");
            shoe = false;
        }

        return shoe;
    }


    public void add() {

        Order o = new Order();

        String lastName = nio.getStringWithNoCommas("Enter the last name ");

        double area = nio.getDouble("Enter the area needed ");

        String userDate = nio.getDateFormattedMMddYYYY("Enter date. Please format: YYYY-MM-DD ");

        String orderSearch = "ORDERS_" + userDate + ".txt";

        Product stuff = setProduct();

        Tax thing = setTax();

        setAndMakeCalculations(stuff, thing, area, lastName, o);

        o.setOrderDate(userDate);

        boolean shoe = makeItHappen(o);

        while (shoe) {
            Order s = orderDao.add(orderSearch, o);
            nio.displayString("\n");

            nio.displayString("Order# | " + s.getOrderNumber() + " Name: " + s.getCustomerName() + " Product: "  + s.getProductType() + "| Total: " + s.getTotal() + "| was added on " + s.getOrderDate());
            shoe = false;
        }

    }

    public String getDate() {
        String stuff = nio.getDateFormattedMMddYYYY("Enter date. Please format: YYYY-MM-DD ");
        String orderSearch = "ORDERS_" + stuff + ".txt";

        return orderSearch;
    }

    public String searchForFile() {
        boolean goon = false;
        String orderSearch ="";
        while (!goon)
        {
            orderSearch = getDate();
            List<Order> order = orderDao.findByDate(orderSearch);
            nio.displayString("Orders for the entered date: ");
            nio.displayString("----------------------------------------------------------------");

            for (Order a : order) {
                nio.displayString("-------------------------------------------------------------");
                nio.displayString("| Order Number: " + a.getOrderNumber() + "| Name: " + a.getCustomerName() + "| Total: " + a.getTotal());
                nio.displayString("\n");
            }
            if (order.isEmpty()) {
                isNotFound();
                nio.displayString("\n");
            } else {
                goon = true;
              }

        }

        return orderSearch;

    }

     public String confirmOrderDetails() {
                String orderSearch = searchForFile();
                String answer = nio.getString("Do you want to view more detail of an individual order? y/n ");
                if ("yes".equalsIgnoreCase(answer) || ("y".equalsIgnoreCase(answer))) {
                    viewSpecificOrderDetails(orderSearch);
                } else {

                }
                return orderSearch;
            }

        public void viewSpecificOrderDetails(String orderSearch) {

                int orderNumber = nio.getInt("What's order number do you want to view? ");

                Order o = new Order();
                o.setOrderNumber(orderNumber);

                Order a = orderDao.findById(orderSearch, o);

                if (a.getCustomerName() == null) {
                    nio.displayString("Id# " + a.getOrderNumber() + " returned no results.");
                    nio.displayString("-----------------------------------------");
                } else {

                    nio.displayString("Order Number: " + a.getOrderNumber() + "| Name: " + a.getCustomerName() + "| State: " + a.getState() +
                            "| Tax Rate: " + a.getTaxRate() + "| Product: " + a.getProductType() + "| Area: " + a.getArea() +
                            "| Cost Per Sq. Ft: " + a.getCostPerSquareFoot() + "| Labor Cost per Sq. Ft: " + a.getLaborCostPerSquareFoot() );
                    nio.displayString("Material Cost: " + a.getMaterialCost() + "| Labor Cost: " + a.getLaborCost() + "| Tax: " + a.getTax() +
                            "| Total: " + a.getTotal());

                }

                nio.getString("Press enter to continue");

        }


    public void edit() {
        boolean go = false;
        boolean confirmDate = false;
        String orderSearch = "";
        while (!confirmDate)
        {
            orderSearch = searchForFile();

            confirmDate = isYourOrderHere();
        }

        int orderNumber = nio.getInt("What's order number do you want to edit? ");

        Order o = new Order();
        o.setOrderNumber(orderNumber);

        Order editOrder = orderDao.findById(orderSearch, o);

        if (editOrder.getCustomerName() == null) {
            nio.displayString("Id# " + editOrder.getOrderNumber() + " returned no results.");
            nio.displayString("-----------------------------------------");
            go = true;
        }
        while (!go)
        {
            nio.displayString("Press enter to leave field as is. ");
            String lastName = nio.getStringLeaveBlank("Enter customer name " + "(" + editOrder.getCustomerName() + " )" + ": ", editOrder.getCustomerName());
            editOrder.setCustomerName(lastName);
            double area = nio.getDoubleLeaveBlank("Enter the area " + "(" + editOrder.getArea() + ")" + ": ", editOrder.getArea());
            editOrder.setArea(area);
            editOrder.setOrderDate(editOrder.getOrderDate());

            Product stuff = setProduct();
            nio.displayString("In controller " + stuff.getProductType() );
            Tax thing = setTax();
            nio.displayString("in cont " + thing.getState());

            setAndMakeCalculations(stuff, thing, area, lastName, editOrder);

            boolean shoe = makeItHappen(o);
            if (shoe) {
                go = true;
            } else if (!shoe) {
                go = true;
            }

            while (shoe) {
                Order fullyEditedOrder = orderDao.update(orderSearch, editOrder);
                nio.displayString(fullyEditedOrder.getCustomerName() + " has been updated. ");
                nio.displayString("\n");
                shoe = false;
                //go=true;
            }

        }

    }

        public void remove() {

        boolean go = false;
        boolean confirmDate = false;
        String orderSearch = "";
        while (!confirmDate) {

        orderSearch = searchForFile();

        confirmDate = isYourOrderHere();

        }

        int orderNumber = nio.getInt("What's order number do you want to delete? ");

        Order o = new Order();
        o.setOrderNumber(orderNumber);

        Order orderDelete = orderDao.findById(orderSearch, o);

        if (orderDelete.getCustomerName() == null) {
            nio.displayString("Id# " + orderDelete.getOrderNumber() + " returned no results.");
            nio.displayString("-----------------------------------------");
            go = true;
        }

        while (!go)
        {

            String confirm = nio.getString("Confirm deletion: y/n ");
            if ("yes".equalsIgnoreCase(confirm) || ("y".equalsIgnoreCase(confirm))) {
                nio.displayString("Order " + orderDelete.getOrderNumber() + " has been deleted. ");
                orderDao.delete(orderSearch, orderDelete);
                go = true;
            } else {
                nio.displayString("Changes aborted ");
                nio.displayString("------------------");
                go = true;

            }
        }

    }

    public void isNotFound() {

        nio.displayString("There are no matches for this date: ");

    }

    public void listProducts() {
        List<Product> myProducts = productDao.list();

        for(Product a : myProducts)

        {
            nio.displayString("-------------------------------------------------------------------------------------------------------------------");
            nio.displayString( "| Product: " + a.getProductType() + "| Cost per Sq. Ft.: " + a.getCostPerSquareFeet() + "| Cost labor per Sq. Ft: " + a.getLaborCostPerSquareFeet());
            nio.displayString("\n");

        }

    }

    public void listTaxes() {
        List<Tax> myTaxes = taxDao.list();

        for(Tax a : myTaxes)

        {
            nio.displayString("-------------------------------------------------------------------------------------------------------------------");
            nio.displayString( "| Id: " + a.getId() + "| State: " + a.getState() + "| Tax rate: " + a.getTaxRate());
            nio.displayString("\n");

        }

    }

    public boolean isYourOrderHere() {
        boolean confirmDate = false;

        nio.displayString("\n");
        nio.displayString("1. Yes");
        nio.displayString("2. No");
        int choice = nio.getIntRange("Is the order number found on this date? ", 2, 1);

        switch (choice) {
            case 1:
                confirmDate = true;
                break;
            case 2:
                confirmDate = false;
                break;

        }

        return confirmDate;
    }

}
