package com.mycompany.dao;

import com.mycompany.dto.Product;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by mymac on 8/30/16.
 */
public class ProductDaoInMemoryImpl implements ProductDao {

    private List<Product> products;
    private static final String TOKEN = ",";

    @Override
    public List<Product> viewProducts() {

        List<Product> myProducts = new ArrayList<>();
        myProducts.add(new Product(1, "Carpet", 2.25, 2.10 ));
        myProducts.add(new Product(2, "Laminate", 1.75, 2.10));
        myProducts.add(new Product(3, "Tile", 3.50, 4.15 ));
        myProducts.add(new Product(4, "Wood", 5.15, 4.75 ));

        return myProducts;

    }


    public ProductDaoInMemoryImpl() {
        products = decode();

    }

    @Override
    public List<Product> list() {

        return new ArrayList(products);
    }

    @Override
    public void encode() {


        PrintWriter out = null;

        try{
            out = new PrintWriter(new FileWriter("PRODUCTS.txt"));

            for(Product s : products)
            {
                out.print(s.getId());
                out.print(TOKEN);

                out.print(s.getProductType());
                out.print(TOKEN);

                out.print(s.getCostPerSquareFeet());
                out.print(TOKEN);

                out.print(s.getLaborCostPerSquareFeet());
                out.print("\n");

            }

            out.flush();


        } catch (IOException ex) {


        } finally {

            out.close();
        }

    }

    @Override
    public List<Product> decode() {

        List<Product> tempProductList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("PRODUCTS.txt")));

            while(sc.hasNextLine() ) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);

                Product myProduct = new Product();
                //Product myProduct = new Product();
                //Tax myTax = new Tax();
                myProduct.setId(Integer.parseInt(stringParts[0]));

                //int on = Integer.parseInt(stringParts[0]);
                myProduct.setProductType(stringParts[1]);
                myProduct.setCostPerSquareFeet(Double.parseDouble(stringParts[2]));
                myProduct.setLaborCostPerSquareFeet(Double.parseDouble(stringParts[3]));

                tempProductList.add(myProduct);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDaoFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tempProductList;
    }

    @Override
    public void createTextFile(File file)  {

        List<Product> products = new ArrayList<>();


        PrintWriter out = null;

        try{
            out = new PrintWriter(new FileWriter(file));

            for(Product s : products)
            {
                out.print(s.getId());
                out.print(TOKEN);

                out.print(s.getProductType());
                out.print(TOKEN);

                out.print(s.getCostPerSquareFeet());
                out.print(TOKEN);

                out.print(s.getLaborCostPerSquareFeet());
                out.print("\n");

            }

            out.flush();

        } catch (IOException ex) {

        } finally {

            out.close();
        }

    }

    @Override
    public Product add(Product product) {

        for (Product p : products)

        {
            if (p.getId() == product.getId()) {
                System.out.println(p.getProductType());

                return p;
            }

        }

        return product;
    }

    }




