package com.mycompany.dao;

import com.mycompany.dto.Tax;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by mymac on 9/6/16.
 */
public interface TaxDao {
    Map<String, Double> viewTaxMap();

    List<Tax> list();

    void encode();

    List<Tax> decode();

    void createTextFile(File file);

    Tax add(Tax tax);
}
