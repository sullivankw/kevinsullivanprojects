package com.mycompany.dao;

import com.mycompany.dto.Product;

import java.io.File;
import java.util.List;

/**
 * Created by mymac on 9/6/16.
 */
public interface ProductDao {
    List<Product> viewProducts();

    List<Product> list();

    void encode();

    List<Product> decode();

    void createTextFile(File file);

    Product add(Product product);
}
