package com.mycompany.dao;

import com.mycompany.dto.Order;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by mymac on 9/1/16.
 */
public interface OrderDao {
    Order add(String orderSearch,  Order order);

    List<Order> findByDate(String orderSearch);

   //// Order updateStep2(Order order);

    //void update( Order order );

    List<Order> list(LocalDate orderDate);

    List<Order> findByName( String customerName);

    void delete(String orderSearch, Order order);

    File findFile();

    //void createCSVFile(File file);

   // List<Order> decodeCSV(File file);

    //void encodeCSV(File file);

    void createDirectory();

    Order findById(String orderSearch, Order order);


    Order update(String orderSearch, Order order);


}
