package com.mycompany.dao;

import com.mycompany.dto.Order;
import com.mycompany.dto.Tax;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.mycompany.dto.Product;

/**
 * Created by mymac on 8/30/16.
 */
public class TaxDaoInMemoryImpl implements TaxDao {


    private static final String TOKEN = ",";
    private List<Tax> taxes;

    public TaxDaoInMemoryImpl() {
        taxes = decode();


    }


    @Override
    public Map<String, Double> viewTaxMap() {

        Map<String, Double> myMap = new HashMap();
        myMap.put("OH", 6.25);
        myMap.put("PA", 6.75);
        myMap.put("MI", 5.75);
        myMap.put("IN", 6.00);

        return myMap;

    }

    @Override
    public List<Tax> list() {

        return new ArrayList(taxes);
    }


    @Override
    public void encode() {


        PrintWriter out = null;

        try{
            out = new PrintWriter(new FileWriter("TAX2.txt"));

            for(Tax s : taxes)
            {
                out.print(s.getId());
                out.print(TOKEN);

                out.print(s.getState());
                out.print(TOKEN);

                out.print(s.getTaxRate());
                out.print("\n");

            }

            out.flush();


        } catch (IOException ex) {


        } finally {

            out.close();
        }

    }

    @Override
    public List<Tax> decode() {

        List<Tax> tempProductList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("TAX2.txt")));

            while(sc.hasNextLine() ) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);

                Tax myTax = new Tax();
                myTax.setId(Integer.parseInt(stringParts[0]));
                myTax.setState(stringParts[1]);
                myTax.setTaxRate(Double.parseDouble(stringParts[2]));

                tempProductList.add(myTax);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDaoFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tempProductList;
    }

    @Override
    public void createTextFile(File file)  {

        List<Order> orders = new ArrayList<>();


        PrintWriter out = null;

        try{
            out = new PrintWriter(new FileWriter(file));

            for(Tax s : taxes)
            {
                out.print(s.getId());
                out.print(TOKEN);

                out.print(s.getState());
                out.print(TOKEN);

                out.print(s.getTaxRate());
                out.print("\n");

            }

            out.flush();

        } catch (IOException ex) {

        } finally {

            out.close();
        }

    }

    @Override
    public Tax add(Tax tax) {


        for (Tax p : taxes)

        {
            if (p.getId() == tax.getId()) {
                System.out.println(p.getState());


                return p;
            }

        }

        return tax;
    }



}