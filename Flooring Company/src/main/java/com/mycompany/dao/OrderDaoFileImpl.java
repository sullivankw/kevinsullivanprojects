package com.mycompany.dao;

import com.mycompany.dto.Order;

import java.io.*;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by mymac on 8/30/16.
 */
public class OrderDaoFileImpl implements OrderDao {


    private List<Order> orders;
    private Integer orderNumber = 1;

    private static final String TOKEN = ",";

    public void encode(File file) {

        PrintWriter out = null;

        try{
            out = new PrintWriter(new FileWriter(file));

            for(Order s : orders)
            {

                out.print(s.getOrderNumber());
                out.print(TOKEN);

                out.print(s.getCustomerName());
                out.print(TOKEN);

                out.print(s.getState());
                out.print(TOKEN);

                out.print(s.getTaxRate());
                out.print(TOKEN);

                out.print(s.getProductType());
                out.print(TOKEN);

                out.print(s.getArea());
                out.print(TOKEN);

                out.print(s.getCostPerSquareFoot());
                out.print(TOKEN);

                out.print(s.getLaborCostPerSquareFoot());
                out.print(TOKEN);

                out.print(s.getMaterialCost());
                out.print(TOKEN);

                out.print(s.getLaborCost());
                out.print(TOKEN);

                out.print(s.getTax());
                out.print(TOKEN);

                out.print(s.getTotal());
                out.print(TOKEN);

                out.print(s.getOrderDate());
                out.print("\n");

            }

            out.flush();


        } catch (IOException ex) {


        } finally {

            out.close();
        }

    }




    @Override
    public void createDirectory() {

           File file = new File("//SCGFlooring");
            if(file.exists()) {
            } else {
                file.mkdir();
            }


            }


    public List<Order> decode(File file) {

        List<Order> tempOrderList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(file)));

            while(sc.hasNextLine() ) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);

                Order myOrder = new Order();
                myOrder.setOrderNumber(Integer.parseInt(stringParts[0]));
                myOrder.setCustomerName(stringParts[1]);
                myOrder.setState(stringParts[2]);
                myOrder.setTaxRate(Double.parseDouble(stringParts[3]));
                myOrder.setProductType(stringParts[4]);
                myOrder.setArea(Double.parseDouble(stringParts[5]));
                myOrder.setCostPerSquareFoot(Double.parseDouble(stringParts[6]));
                myOrder.setLaborCostPerSquareFoot(Double.parseDouble(stringParts[7]));
                myOrder.setLaborCost(Double.parseDouble(stringParts[8]));
                myOrder.setMaterialCost(Double.parseDouble(stringParts[9]));
                myOrder.setTax(Double.parseDouble(stringParts[10]));
                myOrder.setTotal(Double.parseDouble(stringParts[11]));
                myOrder.setOrderDate(stringParts[12]);

                tempOrderList.add(myOrder);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDaoFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tempOrderList;
    }

    @Override
    public Order add(String orderSearch, Order order) {

        createDirectory();

        //File file = new File("/Users/mymac/Documents/guildrepoaug/kevin.sullivan.self.work/labs/Labs Week 5/Flooring Masterey Intel/" +  orderSearch);
        File file = new File("//SCGFlooring/" +  orderSearch);
        if (file.exists()) {
            orders = decode(file);
        } else {
            createTextFile(file);
            //encode(file);
            orders = decode(file);
        }

        order.setOrderNumber(orders.size() + 1);

        orders.add(order);

        encode(file);

        return order;

    }


    public void createTextFile(File file)  {

        PrintWriter out = null;

        try{
            out = new PrintWriter(new FileWriter(file));

            out.flush();

        } catch (IOException ex) {

        } finally {

            out.close();
        }

    }

    @Override
    public List<Order> findByDate(String orderSearch) {
        List<Order> result = new ArrayList();
        File file = new File("//SCGFlooring/" +  orderSearch);

        if (file.exists()) {
            orders = decode(file);
            result = orders;
        }


        return result;

    }

    @Override
    public Order findById(String orderSearch, Order order) {
        List<Order> result = new ArrayList<>();
        File file = new File("//SCGFlooring/" +  orderSearch);
        //File file = new File("/Users/mymac/Documents/guildrepoaug/kevin.sullivan.self.work/labs/Labs Week 5/Flooring Masterey Intel/" +  orderSearch);
        if (file.exists()) {
            orders = decode(file);

        }

        for (Order a : orders)
        {
            if (a.getOrderNumber() == order.getOrderNumber()) {
                return a;

            }
        }

        return order;
    }
    
    public File findFile() {

        return null;
    }

    @Override
    public Order update(String orderSearch, Order order) {
        //File file = new File("/Users/mymac/Documents/guildrepoaug/kevin.sullivan.self.work/labs/Labs Week 5/Flooring Masterey Intel/" +  orderSearch);
        File file = new File("//SCGFlooring/" +  orderSearch);

        encode(file);
        return order;

    }

    @Override
    public List<Order> findByName( String customerName) {
        List<Order> result = new ArrayList();

        for (Order s : orders) {
            if (s.getCustomerName().equals(customerName)) {
                result.add(s);

            }

        }

        return result;

    }

    @Override
    public void delete(String orderSearch, Order order) {

        List<Order> results = new ArrayList<>();
        File file = new File("//SCGFlooring/" +  orderSearch);
        //File file = new File("/Users/mymac/Documents/guildrepoaug/kevin.sullivan.self.work/labs/Labs Week 5/Flooring Masterey Intel/" +  orderSearch);
        if (file.exists()) {
            orders = decode(file);
        }

        Iterator<Order> iter = orders.iterator();

        while (iter.hasNext())
        {
            Order ord = iter.next();

            if (ord.getOrderNumber() == order.getOrderNumber()) {

                iter.remove();
            }
        }

        encode(file);
    }

    @Override
    public List<Order> list(LocalDate orderDate){


        return null;
    }


    }



