package com.mycompany.dao;

import com.mycompany.dto.Order;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

/**
 * Created by mymac on 9/14/16.
 */
public class OrderDaoInMemoryImplTest {

    OrderDao orderDao;
    Order testA = new Order();

    public OrderDaoInMemoryImplTest() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        orderDao = ctx.getBean("flooringDao", OrderDao.class);
    }

    @Before
    public void setUp() {

//        testA.setTaxRate(10);
//        testA.setTax(25);
//        testA.setArea(5);
//        testA.setState("OH");
//        testA.setCostPerSquareFoot(25);
//        testA.setLaborCost(10);
//        testA.setMaterialCost(20);
//        testA.setCustomerName("Bobby");
//        testA.setTotal(200);
//        testA.setProductType("Laminate");
//        testA.setOrderDate("12122009");
//        testA.setOrderNumber(3);
    }


    @Test
    public void add() throws Exception {



        testA.setTaxRate(10);
        testA.setTax(25);
        testA.setArea(5);
        testA.setState("OH");
        testA.setCostPerSquareFoot(25);
        testA.setLaborCost(10);
        testA.setMaterialCost(20);
        testA.setCustomerName("Bobby");
        testA.setTotal(200);
        testA.setProductType("Laminate");
        testA.setOrderDate("12122009");


        Order c = orderDao.add("12122209", testA);

        //Assert.assertEquals("message", 25, c.getTax());
        Assert.assertEquals("message", "Bobby", c.getCustomerName());



    }

    @Test
    public void findByDate() throws Exception {


    }

    @Test
    public void findById() throws Exception {

//        Order g = orderDao.findById("12122009", testA);
//        Assert.assertEquals("message", "Bobby", g.getCustomerName());


    }

    @Test
    public void update() throws Exception {

//        Order g = orderDao.findById("12122009", testA);
//
////        String stuff = nio.getDateFormattedMMddYYYY("Enter date. Please format: YYYY-MM-DD ");
////        String orderSearch = "ORDERS_" + stuff + ".txt";
//        testA.setTaxRate(5);
//        testA.setTax(25);
//        testA.setArea(5);
//        testA.setState("OH");
//        testA.setCostPerSquareFoot(5);
//        testA.setLaborCost(10);
//        testA.setMaterialCost(20);
//        testA.setCustomerName("Bob");
//
//
//
//        Order b = orderDao.update("ORDERS_12122010.txt", testA);
//        //Assert.assertEquals("message", 25, testA.getTax());
//        Assert.assertEquals("message", "Bob", testA.getCustomerName());


    }

    @Test
    public void delete() throws Exception {


    }

}