To launch the program...

Select the downloads link from the nagigation controls to download a zip of this repo. You will need both Flooring Company folder and the ConsoleIO
folder to run the program to run on your respective IDE.

**************************************************************************************************************************************************

This program was created to serve as an order processor for a flooring company

Features:

A. AOP used to time methods and log transactions
B. New text files created based on date entered for order if date doesn't exist in the system
C. Testing environment created using in memory implementation of OrderDao.
D. Validation implemented through the use of try and catch statements found in the CONSOLEIO.
